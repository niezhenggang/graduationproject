package pri.common;


import cn.hutool.core.lang.Snowflake;
import cn.hutool.core.util.IdUtil;

import java.io.*;
import java.net.*;

/**
 * 此类容易引起冲突
 * 如果需要添加修改请提交小组会议讨论
 * 所有方法都static
 * */
public class Common {
    public static final int PAY_ALI = 1;
    public static final int PAY_WX = 2;
    public static final String ORDER_STATUS_1 = "待支付";
    public static final String ORDER_STATUS_2 = "待发货";
    public static final String ORDER_STATUS_3 = "待确认";
    public static final String ORDER_STATUS_4 = "未评价";
    public static final String ORDER_STATUS_5 = "已完成";



    /**
    * 此方法用于模拟get请求
    * @param url | String 填写访问的url
    * @param cookie | String 是否携带cookie
    * @return null | String
    * @author DJY
    * */
    public static String httpGet(String url,String cookie){
        String result = null;
        try {
            URL real_url = new URL(url);
            HttpURLConnection connection = (HttpURLConnection) real_url.openConnection();
            connection.setRequestMethod("GET");
            connection.setConnectTimeout(5000);
            if (cookie != ""){
                connection.setRequestProperty("Cookie",cookie);
            }
            connection.connect();
            if (connection.getResponseCode() == 200) {
                //用getInputStream()方法获得服务器返回的输入流
                InputStream in = connection.getInputStream();
                byte[] data = toByteArray(in); //流转换为二进制数组，read()自己写的是转换方法
                result = new String(data, "UTF-8");
                in.close();
                connection.disconnect();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return result;
    };

    /**
     * 此方法用于模拟post请求
     * @param url | String 填写访问的url
     * @param cookie | String 是否携带cookie
     * @param post | String post数据
     * @author DJY
     * */
    public static String httpPost(String url,String post,String cookie){
        String result = null;
        try{
            URL real_url = new URL(url);
            // 通过远程url连接对象打开连接
            HttpURLConnection connection = (HttpURLConnection) real_url.openConnection();
            // 设置连接请求方式
            connection.setRequestMethod("POST");
            // 设置连接主机服务器超时时间：15000毫秒
            connection.setConnectTimeout(15000);
            // 设置读取主机服务器返回数据超时时间：60000毫秒
            connection.setReadTimeout(60000);
            // 默认值为：false，当向远程服务器传送数据/写数据时，需要设置为true
            connection.setDoOutput(true);
            // 默认值为：true，当前向远程服务读取数据时，设置为true，该参数可有可无
            connection.setDoInput(true);
            // 设置传入参数的格式:请求参数应该是 name1=value1&name2=value2 的形式。
            connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            // 设置鉴权信息：Authorization: Bearer da3efcbf-0845-4fe3-8aba-ee040be542c0
            connection.setRequestProperty("Authorization", "Bearer da3efcbf-0845-4fe3-8aba-ee040be542c0");
            // 设置cookies信息
            if (cookie != ""){
                connection.setRequestProperty("Cookie",cookie);
            }
            // 通过连接对象获取一个输出流
            OutputStream os = connection.getOutputStream();
            // 通过输出流对象将参数写出去/传输出去,它是通过字节数组写出的
            os.write(post.getBytes());
            if (connection.getResponseCode() == 200) {
                InputStream in = connection.getInputStream();
                byte[] data = toByteArray(in); //流转换为二进制数组，read()自己写的是转换方法
                result = new String(data, "UTF-8");
                in.close();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return result;
    };

    /**
     * 此方法用于将 流 转换为 byte[]
     * @param input | InputStream 填写访问的url
     * @author DJY
     * */
    public static byte[] toByteArray(InputStream input) throws IOException {
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        byte[] buffer = new byte[1024*4];
        int n = 0;
        while (-1 != (n = input.read(buffer))) {
            output.write(buffer, 0, n);
        }
        return output.toByteArray();
    }


    public static String getUUID(int len){
        Snowflake snowflake = IdUtil.getSnowflake(1, 2);
        String s = String.valueOf(snowflake.nextId());

       return s.substring(s.length()-len);
    }
}
