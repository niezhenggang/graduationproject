package pri.controller.order;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import pri.pojo.RecAddress;
import pri.service.order.OrderService;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/order")
public class OrderController {
    @Autowired
    OrderService orderService;

    @RequestMapping("/list")
    public String show(){
        return "back/order/order_manage";
    }

    @RequestMapping("/all")
    @ResponseBody
    public Map sendOrder(HttpServletRequest request){
        String op = request.getParameter("op");
        String page = request.getParameter("page");
        String limit = request.getParameter("limit");
        List<Map> maps = null;
        if (op.equals("recent")){
            maps = orderService.getStatusOrder(page,limit,"recent");
        }

        if (op.equals("waitpay")){
            maps = orderService.getStatusOrder(page,limit,"waitpay");
        }

        if (op.equals("waitsend")){
            maps = orderService.getStatusOrder(page,limit,"waitsend");
        }

        if (op.equals("waitconfirm")){
            maps = orderService.getStatusOrder(page,limit,"waitconfirm");
        }


        if (op.equals("waitcomment")){
            maps = orderService.getStatusOrder(page,limit,"waitcomment");
        }

        if (op.equals("success")){
            maps = orderService.getStatusOrder(page,limit,"success");
        }


        if (op.equals("history")){
            maps = orderService.getStatusOrder(page,limit,"history");
        }

        Map<String,Object> table = new HashMap<>();
        table.put("code",0);
        table.put("msg","");
        table.put("count",maps.size());
        table.put("data",maps);
        return table;
    }

    @RequestMapping("/getrec")
    @ResponseBody
    public RecAddress getRecAddress(HttpServletRequest request){
        Map<String,Object> map = new HashMap<>();
        String oid = request.getParameter("oid");
        RecAddress recAddress = orderService.getAddressByOrderId(oid);
        return recAddress;
    }

    @RequestMapping("/send")
    @ResponseBody
    public Map sendProduct(HttpServletRequest request){
        String oid = request.getParameter("oid");
        String com = request.getParameter("com_id");
        String exp_num = request.getParameter("express_num");
        Map<String,Object> map = new HashMap<>();
        if (orderService.sendProduct(oid,com,exp_num)){
            map.put("code","0");
        }else {
            map.put("code","1");
        }
        return map;

    }
}
