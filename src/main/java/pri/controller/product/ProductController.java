package pri.controller.product;



import cn.hutool.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import pri.pojo.*;
import pri.service.product.CategoryService;
import pri.service.product.ImgService;
import pri.service.product.ProductService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/product")
public class ProductController {
    @Autowired
    ProductService productService;
    @Autowired
    CategoryService categoryService;
    @Autowired
    ImgService imgService;


    @GetMapping("/list")
    public String list(Model model){
        System.out.println("接收到一个/list请求");

        return "/back/product/my";
    }

    @RequestMapping("/all")
    @ResponseBody
    public Map<String,Object> list(Model model,int page,int limit,HttpSession session){
        System.out.println("接收到一个/list请求");

        Page page1 = new Page(page,limit);
        List<ProductInfo> products = productService.findByExample(null, page1);

        Map<String,Object> result = new HashMap<>();
        result.put("code",0);
        result.put("msg","");
        result.put("count",page1.getTotalRows());
        result.put("data",products);

        return result;
    }

    @RequestMapping("/detail/{id}")
    public String detail(@PathVariable Integer id, Model model,  HttpSession session){
        User loginUser = (User) session.getAttribute("user");
        String userId = null;
        if(loginUser!=null){
            userId = loginUser.getUserId();
        }

        model.addAttribute("product",productService.findById(id,userId));

        return "/back/product/detail";
    }



    @GetMapping("/add")
    public String add(Model model){
        model.addAttribute("category",categoryService.findByExample(null, null));
        model.addAttribute("product",new ProductInfo());

        return "/back/product/add";
    }

    @PostMapping("/add")
    public String add(ProductInfo product){
        System.out.println("接收到一个/add请求，参数为:"+product);

        Integer add = productService.add(product);

        if(add>0){
            return "redirect:list";
        }

        return "/back/product/add";
    }

    @PostMapping("/addImg")
    @ResponseBody
    public JSONObject addImg(MultipartFile file, HttpSession session){
        System.out.println("接收到一个addimg请求："+file.getOriginalFilename());

        String realPath = session.getServletContext().getRealPath("/");
        String url = imgService.saveImg(realPath, file);

        JSONObject src = new JSONObject();
        src.put("src",url);

        JSONObject result = new JSONObject();
        result.put("code",0);
        result.put("msg","");
        result.put("data",src);

        return result;
    }


    @PostMapping("/addImgs")
    @ResponseBody
    public JSONObject addImgs(List<MultipartFile> imgFiles, HttpSession session){
        System.out.println("接收到一个addimgs请求："+imgFiles);

        String realPath = session.getServletContext().getRealPath("/");
        List<String> urls = imgService.saveImgs(realPath, imgFiles);

        JSONObject result = new JSONObject();
        result.put("code",0);
        result.put("msg","");
        result.put("data",urls);

        return result;
    }



    @GetMapping("/update")
    public String update(Model model){

        return "/back/product/update";
    }

    @PostMapping("/update")
    @ResponseBody
    public boolean update(Product product){


        return productService.update(product);
    }

    @GetMapping("/delete")
    public String delete(Model model){

        return "/back/product/delete";
    }

    @PostMapping("/delete")
    @ResponseBody
    public boolean delete(Integer id){

        return productService.delete(id);
    }
}
