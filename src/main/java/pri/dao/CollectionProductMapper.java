package pri.dao;

import java.util.List;
import org.apache.ibatis.annotations.Param;
import pri.pojo.CollectionProduct;
import pri.pojo.CollectionProductExample;

public interface CollectionProductMapper {
    long countByExample(CollectionProductExample example);

    int deleteByExample(CollectionProductExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(CollectionProduct record);

    int insertSelective(CollectionProduct record);

    List<CollectionProduct> selectByExample(CollectionProductExample example);

    CollectionProduct selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") CollectionProduct record, @Param("example") CollectionProductExample example);

    int updateByExample(@Param("record") CollectionProduct record, @Param("example") CollectionProductExample example);

    int updateByPrimaryKeySelective(CollectionProduct record);

    int updateByPrimaryKey(CollectionProduct record);
}