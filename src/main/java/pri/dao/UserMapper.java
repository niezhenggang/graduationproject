package pri.dao;

import org.apache.ibatis.annotations.Param;
import pri.pojo.User;
import pri.pojo.UserExample;

import java.util.List;

public interface UserMapper {
    //统计
    long countByExample(UserExample example);
    //按条件删除
    int deleteByExample(UserExample example);
    // 按主键删除
    int deleteByPrimaryKey(String userId);
    //插入
    int insert(User record);
    //按条件插入
    int insertSelective(User record);
    //按条件选择
    List<User> selectByExample(UserExample example);
    //按主键选择
    User selectByPrimaryKey(String userId);
    //按条件部分更新
    int updateByExampleSelective(@Param("record") User record, @Param("example") UserExample example);
    // 按条件全部更新
    int updateByExample(@Param("record") User record, @Param("example") UserExample example);
    //按主键部分更新
    int updateByPrimaryKeySelective(User record);
    //按主键更新
    int updateByPrimaryKey(User record);
}