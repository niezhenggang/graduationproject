package pri.pojo;

public class SalePercent {
    private String categoryName;
    private String percent;

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getPercent() {
        return percent;
    }

    public void setPercent(String percent) {
        this.percent = percent;
    }

    @Override
    public String toString() {
        return "SalePercent{" +
                "categoryName='" + categoryName + '\'' +
                ", percent='" + percent + '\'' +
                '}';
    }
}
