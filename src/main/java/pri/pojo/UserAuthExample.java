package pri.pojo;

import java.util.ArrayList;
import java.util.List;

public class UserAuthExample {
    protected String orderByClause;

    protected boolean distinct;

    protected int startRow;

    protected int pageRows;

    protected List<Criteria> oredCriteria;

    public UserAuthExample() {
        oredCriteria = new ArrayList<>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public void setStartRow(int startRow) {
        this.startRow = startRow;
    }

    public int getStartRow() {
        return startRow;
    }

    public void setPageRows(int pageRows) {
        this.pageRows = pageRows;
    }

    public int getPageRows() {
        return pageRows;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andOpeanIdIsNull() {
            addCriterion("opean_id is null");
            return (Criteria) this;
        }

        public Criteria andOpeanIdIsNotNull() {
            addCriterion("opean_id is not null");
            return (Criteria) this;
        }

        public Criteria andOpeanIdEqualTo(String value) {
            addCriterion("opean_id =", value, "opeanId");
            return (Criteria) this;
        }

        public Criteria andOpeanIdNotEqualTo(String value) {
            addCriterion("opean_id <>", value, "opeanId");
            return (Criteria) this;
        }

        public Criteria andOpeanIdGreaterThan(String value) {
            addCriterion("opean_id >", value, "opeanId");
            return (Criteria) this;
        }

        public Criteria andOpeanIdGreaterThanOrEqualTo(String value) {
            addCriterion("opean_id >=", value, "opeanId");
            return (Criteria) this;
        }

        public Criteria andOpeanIdLessThan(String value) {
            addCriterion("opean_id <", value, "opeanId");
            return (Criteria) this;
        }

        public Criteria andOpeanIdLessThanOrEqualTo(String value) {
            addCriterion("opean_id <=", value, "opeanId");
            return (Criteria) this;
        }

        public Criteria andOpeanIdLike(String value) {
            addCriterion("opean_id like", value, "opeanId");
            return (Criteria) this;
        }

        public Criteria andOpeanIdNotLike(String value) {
            addCriterion("opean_id not like", value, "opeanId");
            return (Criteria) this;
        }

        public Criteria andOpeanIdIn(List<String> values) {
            addCriterion("opean_id in", values, "opeanId");
            return (Criteria) this;
        }

        public Criteria andOpeanIdNotIn(List<String> values) {
            addCriterion("opean_id not in", values, "opeanId");
            return (Criteria) this;
        }

        public Criteria andOpeanIdBetween(String value1, String value2) {
            addCriterion("opean_id between", value1, value2, "opeanId");
            return (Criteria) this;
        }

        public Criteria andOpeanIdNotBetween(String value1, String value2) {
            addCriterion("opean_id not between", value1, value2, "opeanId");
            return (Criteria) this;
        }

        public Criteria andUserIdIsNull() {
            addCriterion("user_id is null");
            return (Criteria) this;
        }

        public Criteria andUserIdIsNotNull() {
            addCriterion("user_id is not null");
            return (Criteria) this;
        }

        public Criteria andUserIdEqualTo(String value) {
            addCriterion("user_id =", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdNotEqualTo(String value) {
            addCriterion("user_id <>", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdGreaterThan(String value) {
            addCriterion("user_id >", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdGreaterThanOrEqualTo(String value) {
            addCriterion("user_id >=", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdLessThan(String value) {
            addCriterion("user_id <", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdLessThanOrEqualTo(String value) {
            addCriterion("user_id <=", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdLike(String value) {
            addCriterion("user_id like", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdNotLike(String value) {
            addCriterion("user_id not like", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdIn(List<String> values) {
            addCriterion("user_id in", values, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdNotIn(List<String> values) {
            addCriterion("user_id not in", values, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdBetween(String value1, String value2) {
            addCriterion("user_id between", value1, value2, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdNotBetween(String value1, String value2) {
            addCriterion("user_id not between", value1, value2, "userId");
            return (Criteria) this;
        }

        public Criteria andQqImgIsNull() {
            addCriterion("qq_img is null");
            return (Criteria) this;
        }

        public Criteria andQqImgIsNotNull() {
            addCriterion("qq_img is not null");
            return (Criteria) this;
        }

        public Criteria andQqImgEqualTo(String value) {
            addCriterion("qq_img =", value, "qqImg");
            return (Criteria) this;
        }

        public Criteria andQqImgNotEqualTo(String value) {
            addCriterion("qq_img <>", value, "qqImg");
            return (Criteria) this;
        }

        public Criteria andQqImgGreaterThan(String value) {
            addCriterion("qq_img >", value, "qqImg");
            return (Criteria) this;
        }

        public Criteria andQqImgGreaterThanOrEqualTo(String value) {
            addCriterion("qq_img >=", value, "qqImg");
            return (Criteria) this;
        }

        public Criteria andQqImgLessThan(String value) {
            addCriterion("qq_img <", value, "qqImg");
            return (Criteria) this;
        }

        public Criteria andQqImgLessThanOrEqualTo(String value) {
            addCriterion("qq_img <=", value, "qqImg");
            return (Criteria) this;
        }

        public Criteria andQqImgLike(String value) {
            addCriterion("qq_img like", value, "qqImg");
            return (Criteria) this;
        }

        public Criteria andQqImgNotLike(String value) {
            addCriterion("qq_img not like", value, "qqImg");
            return (Criteria) this;
        }

        public Criteria andQqImgIn(List<String> values) {
            addCriterion("qq_img in", values, "qqImg");
            return (Criteria) this;
        }

        public Criteria andQqImgNotIn(List<String> values) {
            addCriterion("qq_img not in", values, "qqImg");
            return (Criteria) this;
        }

        public Criteria andQqImgBetween(String value1, String value2) {
            addCriterion("qq_img between", value1, value2, "qqImg");
            return (Criteria) this;
        }

        public Criteria andQqImgNotBetween(String value1, String value2) {
            addCriterion("qq_img not between", value1, value2, "qqImg");
            return (Criteria) this;
        }

        public Criteria andNickNameIsNull() {
            addCriterion("nick_name is null");
            return (Criteria) this;
        }

        public Criteria andNickNameIsNotNull() {
            addCriterion("nick_name is not null");
            return (Criteria) this;
        }

        public Criteria andNickNameEqualTo(String value) {
            addCriterion("nick_name =", value, "nickName");
            return (Criteria) this;
        }

        public Criteria andNickNameNotEqualTo(String value) {
            addCriterion("nick_name <>", value, "nickName");
            return (Criteria) this;
        }

        public Criteria andNickNameGreaterThan(String value) {
            addCriterion("nick_name >", value, "nickName");
            return (Criteria) this;
        }

        public Criteria andNickNameGreaterThanOrEqualTo(String value) {
            addCriterion("nick_name >=", value, "nickName");
            return (Criteria) this;
        }

        public Criteria andNickNameLessThan(String value) {
            addCriterion("nick_name <", value, "nickName");
            return (Criteria) this;
        }

        public Criteria andNickNameLessThanOrEqualTo(String value) {
            addCriterion("nick_name <=", value, "nickName");
            return (Criteria) this;
        }

        public Criteria andNickNameLike(String value) {
            addCriterion("nick_name like", value, "nickName");
            return (Criteria) this;
        }

        public Criteria andNickNameNotLike(String value) {
            addCriterion("nick_name not like", value, "nickName");
            return (Criteria) this;
        }

        public Criteria andNickNameIn(List<String> values) {
            addCriterion("nick_name in", values, "nickName");
            return (Criteria) this;
        }

        public Criteria andNickNameNotIn(List<String> values) {
            addCriterion("nick_name not in", values, "nickName");
            return (Criteria) this;
        }

        public Criteria andNickNameBetween(String value1, String value2) {
            addCriterion("nick_name between", value1, value2, "nickName");
            return (Criteria) this;
        }

        public Criteria andNickNameNotBetween(String value1, String value2) {
            addCriterion("nick_name not between", value1, value2, "nickName");
            return (Criteria) this;
        }

        public Criteria andMultiseriateOrLike(java.util.HashMap<String, String> map) {
            if(map.isEmpty()) return (Criteria) this;
            StringBuilder sb = new StringBuilder();
            sb.append("(");
            java.util.Set<String> keySet = map.keySet();
            for (String str : keySet) {
                sb.append(" or "+str+" like '%%"+map.get(str)+"%%'");
            }
            sb.append(")");
            int index = sb.indexOf("or");
            sb.delete(index, index+2);
            addCriterion(sb.toString());
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {
        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}