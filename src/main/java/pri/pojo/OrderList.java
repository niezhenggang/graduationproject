package pri.pojo;

import java.util.Date;

public class OrderList {
    private Integer id;

    private String userId;

    private String orderId;

    private String status;

    private String total;

    private Date createtime1;

    private Date createtime2;

    private Date createtime3;

    private Date createtime4;

    private Date createtime5;

    private Date createtime6;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId == null ? null : userId.trim();
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId == null ? null : orderId.trim();
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status == null ? null : status.trim();
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total == null ? null : total.trim();
    }

    public Date getCreatetime1() {
        return createtime1;
    }

    public void setCreatetime1(Date createtime1) {
        this.createtime1 = createtime1;
    }

    public Date getCreatetime2() {
        return createtime2;
    }

    public void setCreatetime2(Date createtime2) {
        this.createtime2 = createtime2;
    }

    public Date getCreatetime3() {
        return createtime3;
    }

    public void setCreatetime3(Date createtime3) {
        this.createtime3 = createtime3;
    }

    public Date getCreatetime4() {
        return createtime4;
    }

    public void setCreatetime4(Date createtime4) {
        this.createtime4 = createtime4;
    }

    public Date getCreatetime5() {
        return createtime5;
    }

    public void setCreatetime5(Date createtime5) {
        this.createtime5 = createtime5;
    }

    public Date getCreatetime6() {
        return createtime6;
    }

    public void setCreatetime6(Date createtime6) {
        this.createtime6 = createtime6;
    }
}