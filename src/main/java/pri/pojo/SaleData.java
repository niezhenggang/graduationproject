package pri.pojo;

public class SaleData {
    private String category_id;
    private String count;

    public String getCategory_id() {
        return category_id;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    @Override
    public String toString() {
        return "SaleData{" +
                "category_id='" + category_id + '\'' +
                ", count='" + count + '\'' +
                '}';
    }
}
