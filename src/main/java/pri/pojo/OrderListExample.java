package pri.pojo;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class OrderListExample {
    protected String orderByClause;

    protected boolean distinct;

    protected int startRow;

    protected int pageRows;

    protected List<Criteria> oredCriteria;

    public OrderListExample() {
        oredCriteria = new ArrayList<>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public void setStartRow(int startRow) {
        this.startRow = startRow;
    }

    public int getStartRow() {
        return startRow;
    }

    public void setPageRows(int pageRows) {
        this.pageRows = pageRows;
    }

    public int getPageRows() {
        return pageRows;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andUserIdIsNull() {
            addCriterion("user_id is null");
            return (Criteria) this;
        }

        public Criteria andUserIdIsNotNull() {
            addCriterion("user_id is not null");
            return (Criteria) this;
        }

        public Criteria andUserIdEqualTo(String value) {
            addCriterion("user_id =", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdNotEqualTo(String value) {
            addCriterion("user_id <>", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdGreaterThan(String value) {
            addCriterion("user_id >", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdGreaterThanOrEqualTo(String value) {
            addCriterion("user_id >=", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdLessThan(String value) {
            addCriterion("user_id <", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdLessThanOrEqualTo(String value) {
            addCriterion("user_id <=", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdLike(String value) {
            addCriterion("user_id like", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdNotLike(String value) {
            addCriterion("user_id not like", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdIn(List<String> values) {
            addCriterion("user_id in", values, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdNotIn(List<String> values) {
            addCriterion("user_id not in", values, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdBetween(String value1, String value2) {
            addCriterion("user_id between", value1, value2, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdNotBetween(String value1, String value2) {
            addCriterion("user_id not between", value1, value2, "userId");
            return (Criteria) this;
        }

        public Criteria andOrderIdIsNull() {
            addCriterion("order_id is null");
            return (Criteria) this;
        }

        public Criteria andOrderIdIsNotNull() {
            addCriterion("order_id is not null");
            return (Criteria) this;
        }

        public Criteria andOrderIdEqualTo(String value) {
            addCriterion("order_id =", value, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderIdNotEqualTo(String value) {
            addCriterion("order_id <>", value, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderIdGreaterThan(String value) {
            addCriterion("order_id >", value, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderIdGreaterThanOrEqualTo(String value) {
            addCriterion("order_id >=", value, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderIdLessThan(String value) {
            addCriterion("order_id <", value, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderIdLessThanOrEqualTo(String value) {
            addCriterion("order_id <=", value, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderIdLike(String value) {
            addCriterion("order_id like", value, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderIdNotLike(String value) {
            addCriterion("order_id not like", value, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderIdIn(List<String> values) {
            addCriterion("order_id in", values, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderIdNotIn(List<String> values) {
            addCriterion("order_id not in", values, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderIdBetween(String value1, String value2) {
            addCriterion("order_id between", value1, value2, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderIdNotBetween(String value1, String value2) {
            addCriterion("order_id not between", value1, value2, "orderId");
            return (Criteria) this;
        }

        public Criteria andStatusIsNull() {
            addCriterion("status is null");
            return (Criteria) this;
        }

        public Criteria andStatusIsNotNull() {
            addCriterion("status is not null");
            return (Criteria) this;
        }

        public Criteria andStatusEqualTo(String value) {
            addCriterion("status =", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotEqualTo(String value) {
            addCriterion("status <>", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThan(String value) {
            addCriterion("status >", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThanOrEqualTo(String value) {
            addCriterion("status >=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThan(String value) {
            addCriterion("status <", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThanOrEqualTo(String value) {
            addCriterion("status <=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLike(String value) {
            addCriterion("status like", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotLike(String value) {
            addCriterion("status not like", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusIn(List<String> values) {
            addCriterion("status in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotIn(List<String> values) {
            addCriterion("status not in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusBetween(String value1, String value2) {
            addCriterion("status between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotBetween(String value1, String value2) {
            addCriterion("status not between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andTotalIsNull() {
            addCriterion("total is null");
            return (Criteria) this;
        }

        public Criteria andTotalIsNotNull() {
            addCriterion("total is not null");
            return (Criteria) this;
        }

        public Criteria andTotalEqualTo(String value) {
            addCriterion("total =", value, "total");
            return (Criteria) this;
        }

        public Criteria andTotalNotEqualTo(String value) {
            addCriterion("total <>", value, "total");
            return (Criteria) this;
        }

        public Criteria andTotalGreaterThan(String value) {
            addCriterion("total >", value, "total");
            return (Criteria) this;
        }

        public Criteria andTotalGreaterThanOrEqualTo(String value) {
            addCriterion("total >=", value, "total");
            return (Criteria) this;
        }

        public Criteria andTotalLessThan(String value) {
            addCriterion("total <", value, "total");
            return (Criteria) this;
        }

        public Criteria andTotalLessThanOrEqualTo(String value) {
            addCriterion("total <=", value, "total");
            return (Criteria) this;
        }

        public Criteria andTotalLike(String value) {
            addCriterion("total like", value, "total");
            return (Criteria) this;
        }

        public Criteria andTotalNotLike(String value) {
            addCriterion("total not like", value, "total");
            return (Criteria) this;
        }

        public Criteria andTotalIn(List<String> values) {
            addCriterion("total in", values, "total");
            return (Criteria) this;
        }

        public Criteria andTotalNotIn(List<String> values) {
            addCriterion("total not in", values, "total");
            return (Criteria) this;
        }

        public Criteria andTotalBetween(String value1, String value2) {
            addCriterion("total between", value1, value2, "total");
            return (Criteria) this;
        }

        public Criteria andTotalNotBetween(String value1, String value2) {
            addCriterion("total not between", value1, value2, "total");
            return (Criteria) this;
        }

        public Criteria andCreatetime1IsNull() {
            addCriterion("createtime1 is null");
            return (Criteria) this;
        }

        public Criteria andCreatetime1IsNotNull() {
            addCriterion("createtime1 is not null");
            return (Criteria) this;
        }

        public Criteria andCreatetime1EqualTo(Date value) {
            addCriterion("createtime1 =", value, "createtime1");
            return (Criteria) this;
        }

        public Criteria andCreatetime1NotEqualTo(Date value) {
            addCriterion("createtime1 <>", value, "createtime1");
            return (Criteria) this;
        }

        public Criteria andCreatetime1GreaterThan(Date value) {
            addCriterion("createtime1 >", value, "createtime1");
            return (Criteria) this;
        }

        public Criteria andCreatetime1GreaterThanOrEqualTo(Date value) {
            addCriterion("createtime1 >=", value, "createtime1");
            return (Criteria) this;
        }

        public Criteria andCreatetime1LessThan(Date value) {
            addCriterion("createtime1 <", value, "createtime1");
            return (Criteria) this;
        }

        public Criteria andCreatetime1LessThanOrEqualTo(Date value) {
            addCriterion("createtime1 <=", value, "createtime1");
            return (Criteria) this;
        }

        public Criteria andCreatetime1In(List<Date> values) {
            addCriterion("createtime1 in", values, "createtime1");
            return (Criteria) this;
        }

        public Criteria andCreatetime1NotIn(List<Date> values) {
            addCriterion("createtime1 not in", values, "createtime1");
            return (Criteria) this;
        }

        public Criteria andCreatetime1Between(Date value1, Date value2) {
            addCriterion("createtime1 between", value1, value2, "createtime1");
            return (Criteria) this;
        }

        public Criteria andCreatetime1NotBetween(Date value1, Date value2) {
            addCriterion("createtime1 not between", value1, value2, "createtime1");
            return (Criteria) this;
        }

        public Criteria andCreatetime2IsNull() {
            addCriterion("createtime2 is null");
            return (Criteria) this;
        }

        public Criteria andCreatetime2IsNotNull() {
            addCriterion("createtime2 is not null");
            return (Criteria) this;
        }

        public Criteria andCreatetime2EqualTo(Date value) {
            addCriterion("createtime2 =", value, "createtime2");
            return (Criteria) this;
        }

        public Criteria andCreatetime2NotEqualTo(Date value) {
            addCriterion("createtime2 <>", value, "createtime2");
            return (Criteria) this;
        }

        public Criteria andCreatetime2GreaterThan(Date value) {
            addCriterion("createtime2 >", value, "createtime2");
            return (Criteria) this;
        }

        public Criteria andCreatetime2GreaterThanOrEqualTo(Date value) {
            addCriterion("createtime2 >=", value, "createtime2");
            return (Criteria) this;
        }

        public Criteria andCreatetime2LessThan(Date value) {
            addCriterion("createtime2 <", value, "createtime2");
            return (Criteria) this;
        }

        public Criteria andCreatetime2LessThanOrEqualTo(Date value) {
            addCriterion("createtime2 <=", value, "createtime2");
            return (Criteria) this;
        }

        public Criteria andCreatetime2In(List<Date> values) {
            addCriterion("createtime2 in", values, "createtime2");
            return (Criteria) this;
        }

        public Criteria andCreatetime2NotIn(List<Date> values) {
            addCriterion("createtime2 not in", values, "createtime2");
            return (Criteria) this;
        }

        public Criteria andCreatetime2Between(Date value1, Date value2) {
            addCriterion("createtime2 between", value1, value2, "createtime2");
            return (Criteria) this;
        }

        public Criteria andCreatetime2NotBetween(Date value1, Date value2) {
            addCriterion("createtime2 not between", value1, value2, "createtime2");
            return (Criteria) this;
        }

        public Criteria andCreatetime3IsNull() {
            addCriterion("createtime3 is null");
            return (Criteria) this;
        }

        public Criteria andCreatetime3IsNotNull() {
            addCriterion("createtime3 is not null");
            return (Criteria) this;
        }

        public Criteria andCreatetime3EqualTo(Date value) {
            addCriterion("createtime3 =", value, "createtime3");
            return (Criteria) this;
        }

        public Criteria andCreatetime3NotEqualTo(Date value) {
            addCriterion("createtime3 <>", value, "createtime3");
            return (Criteria) this;
        }

        public Criteria andCreatetime3GreaterThan(Date value) {
            addCriterion("createtime3 >", value, "createtime3");
            return (Criteria) this;
        }

        public Criteria andCreatetime3GreaterThanOrEqualTo(Date value) {
            addCriterion("createtime3 >=", value, "createtime3");
            return (Criteria) this;
        }

        public Criteria andCreatetime3LessThan(Date value) {
            addCriterion("createtime3 <", value, "createtime3");
            return (Criteria) this;
        }

        public Criteria andCreatetime3LessThanOrEqualTo(Date value) {
            addCriterion("createtime3 <=", value, "createtime3");
            return (Criteria) this;
        }

        public Criteria andCreatetime3In(List<Date> values) {
            addCriterion("createtime3 in", values, "createtime3");
            return (Criteria) this;
        }

        public Criteria andCreatetime3NotIn(List<Date> values) {
            addCriterion("createtime3 not in", values, "createtime3");
            return (Criteria) this;
        }

        public Criteria andCreatetime3Between(Date value1, Date value2) {
            addCriterion("createtime3 between", value1, value2, "createtime3");
            return (Criteria) this;
        }

        public Criteria andCreatetime3NotBetween(Date value1, Date value2) {
            addCriterion("createtime3 not between", value1, value2, "createtime3");
            return (Criteria) this;
        }

        public Criteria andCreatetime4IsNull() {
            addCriterion("createtime4 is null");
            return (Criteria) this;
        }

        public Criteria andCreatetime4IsNotNull() {
            addCriterion("createtime4 is not null");
            return (Criteria) this;
        }

        public Criteria andCreatetime4EqualTo(Date value) {
            addCriterion("createtime4 =", value, "createtime4");
            return (Criteria) this;
        }

        public Criteria andCreatetime4NotEqualTo(Date value) {
            addCriterion("createtime4 <>", value, "createtime4");
            return (Criteria) this;
        }

        public Criteria andCreatetime4GreaterThan(Date value) {
            addCriterion("createtime4 >", value, "createtime4");
            return (Criteria) this;
        }

        public Criteria andCreatetime4GreaterThanOrEqualTo(Date value) {
            addCriterion("createtime4 >=", value, "createtime4");
            return (Criteria) this;
        }

        public Criteria andCreatetime4LessThan(Date value) {
            addCriterion("createtime4 <", value, "createtime4");
            return (Criteria) this;
        }

        public Criteria andCreatetime4LessThanOrEqualTo(Date value) {
            addCriterion("createtime4 <=", value, "createtime4");
            return (Criteria) this;
        }

        public Criteria andCreatetime4In(List<Date> values) {
            addCriterion("createtime4 in", values, "createtime4");
            return (Criteria) this;
        }

        public Criteria andCreatetime4NotIn(List<Date> values) {
            addCriterion("createtime4 not in", values, "createtime4");
            return (Criteria) this;
        }

        public Criteria andCreatetime4Between(Date value1, Date value2) {
            addCriterion("createtime4 between", value1, value2, "createtime4");
            return (Criteria) this;
        }

        public Criteria andCreatetime4NotBetween(Date value1, Date value2) {
            addCriterion("createtime4 not between", value1, value2, "createtime4");
            return (Criteria) this;
        }

        public Criteria andCreatetime5IsNull() {
            addCriterion("createtime5 is null");
            return (Criteria) this;
        }

        public Criteria andCreatetime5IsNotNull() {
            addCriterion("createtime5 is not null");
            return (Criteria) this;
        }

        public Criteria andCreatetime5EqualTo(Date value) {
            addCriterion("createtime5 =", value, "createtime5");
            return (Criteria) this;
        }

        public Criteria andCreatetime5NotEqualTo(Date value) {
            addCriterion("createtime5 <>", value, "createtime5");
            return (Criteria) this;
        }

        public Criteria andCreatetime5GreaterThan(Date value) {
            addCriterion("createtime5 >", value, "createtime5");
            return (Criteria) this;
        }

        public Criteria andCreatetime5GreaterThanOrEqualTo(Date value) {
            addCriterion("createtime5 >=", value, "createtime5");
            return (Criteria) this;
        }

        public Criteria andCreatetime5LessThan(Date value) {
            addCriterion("createtime5 <", value, "createtime5");
            return (Criteria) this;
        }

        public Criteria andCreatetime5LessThanOrEqualTo(Date value) {
            addCriterion("createtime5 <=", value, "createtime5");
            return (Criteria) this;
        }

        public Criteria andCreatetime5In(List<Date> values) {
            addCriterion("createtime5 in", values, "createtime5");
            return (Criteria) this;
        }

        public Criteria andCreatetime5NotIn(List<Date> values) {
            addCriterion("createtime5 not in", values, "createtime5");
            return (Criteria) this;
        }

        public Criteria andCreatetime5Between(Date value1, Date value2) {
            addCriterion("createtime5 between", value1, value2, "createtime5");
            return (Criteria) this;
        }

        public Criteria andCreatetime5NotBetween(Date value1, Date value2) {
            addCriterion("createtime5 not between", value1, value2, "createtime5");
            return (Criteria) this;
        }

        public Criteria andCreatetime6IsNull() {
            addCriterion("createtime6 is null");
            return (Criteria) this;
        }

        public Criteria andCreatetime6IsNotNull() {
            addCriterion("createtime6 is not null");
            return (Criteria) this;
        }

        public Criteria andCreatetime6EqualTo(Date value) {
            addCriterion("createtime6 =", value, "createtime6");
            return (Criteria) this;
        }

        public Criteria andCreatetime6NotEqualTo(Date value) {
            addCriterion("createtime6 <>", value, "createtime6");
            return (Criteria) this;
        }

        public Criteria andCreatetime6GreaterThan(Date value) {
            addCriterion("createtime6 >", value, "createtime6");
            return (Criteria) this;
        }

        public Criteria andCreatetime6GreaterThanOrEqualTo(Date value) {
            addCriterion("createtime6 >=", value, "createtime6");
            return (Criteria) this;
        }

        public Criteria andCreatetime6LessThan(Date value) {
            addCriterion("createtime6 <", value, "createtime6");
            return (Criteria) this;
        }

        public Criteria andCreatetime6LessThanOrEqualTo(Date value) {
            addCriterion("createtime6 <=", value, "createtime6");
            return (Criteria) this;
        }

        public Criteria andCreatetime6In(List<Date> values) {
            addCriterion("createtime6 in", values, "createtime6");
            return (Criteria) this;
        }

        public Criteria andCreatetime6NotIn(List<Date> values) {
            addCriterion("createtime6 not in", values, "createtime6");
            return (Criteria) this;
        }

        public Criteria andCreatetime6Between(Date value1, Date value2) {
            addCriterion("createtime6 between", value1, value2, "createtime6");
            return (Criteria) this;
        }

        public Criteria andCreatetime6NotBetween(Date value1, Date value2) {
            addCriterion("createtime6 not between", value1, value2, "createtime6");
            return (Criteria) this;
        }

        public Criteria andMultiseriateOrLike(java.util.HashMap<String, String> map) {
            if(map.isEmpty()) return (Criteria) this;
            StringBuilder sb = new StringBuilder();
            sb.append("(");
            java.util.Set<String> keySet = map.keySet();
            for (String str : keySet) {
                sb.append(" or "+str+" like '%%"+map.get(str)+"%%'");
            }
            sb.append(")");
            int index = sb.indexOf("or");
            sb.delete(index, index+2);
            addCriterion(sb.toString());
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {
        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}