package pri.pojo;

public class UserAuth {
    private String opeanId;

    private String userId;

    private String qqImg;

    private String nickName;

    public String getOpeanId() {
        return opeanId;
    }

    public void setOpeanId(String opeanId) {
        this.opeanId = opeanId == null ? null : opeanId.trim();
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId == null ? null : userId.trim();
    }

    public String getQqImg() {
        return qqImg;
    }

    public void setQqImg(String qqImg) {
        this.qqImg = qqImg == null ? null : qqImg.trim();
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName == null ? null : nickName.trim();
    }
}