package pri.pojo;

public class Coupon {
    private Integer couponId;

    private Integer preferentialSum;

    private Integer minimumConsumptionSum;

    private String userId;

    private Integer scope;

    private Integer targetId;

    private String startTime;

    private String deadline;

    private String detail;

    private Integer status;

    private String target;

    public Integer getCouponId() {
        return couponId;
    }

    public void setCouponId(Integer couponId) {
        this.couponId = couponId;
    }

    public Integer getPreferentialSum() {
        return preferentialSum;
    }

    public void setPreferentialSum(Integer preferentialSum) {
        this.preferentialSum = preferentialSum;
    }

    public Integer getMinimumConsumptionSum() {
        return minimumConsumptionSum;
    }

    public void setMinimumConsumptionSum(Integer minimumConsumptionSum) {
        this.minimumConsumptionSum = minimumConsumptionSum;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId == null ? null : userId.trim();
    }

    public Integer getScope() {
        return scope;
    }

    public void setScope(Integer scope) {
        this.scope = scope;
    }

    public Integer getTargetId() {
        return targetId;
    }

    public void setTargetId(Integer targetId) {
        this.targetId = targetId;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime == null ? null : startTime.trim();
    }

    public String getDeadline() {
        return deadline;
    }

    public void setDeadline(String deadline) {
        this.deadline = deadline == null ? null : deadline.trim();
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail == null ? null : detail.trim();
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }
}