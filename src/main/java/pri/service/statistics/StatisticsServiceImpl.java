package pri.service.statistics;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pri.dao.CategoryMapper;
import pri.pojo.SaleData;
import pri.pojo.SalePercent;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
public class StatisticsServiceImpl implements StatisticsService{

    @Autowired
    private CategoryMapper categoryMapper;

    @Override
    public List<SalePercent> getSalePercents(List<SaleData> saleDatas, int totalSaleNum) {
        List<SalePercent> salePercents=new ArrayList<>();

        double percent;
        for (SaleData sd:saleDatas){
            SalePercent salePercent=new SalePercent();
            salePercent.setCategoryName(categoryMapper.selectByPrimaryKey(Integer.parseInt(sd.getCategory_id())).getName());
            percent=Double.parseDouble(sd.getCount())/totalSaleNum*100;
            salePercent.setPercent(String.format("%.0f",percent)+"%");   //转换成百分数
            salePercents.add(salePercent);
        }

        return salePercents;
    }
}
