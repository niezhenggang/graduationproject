package pri.service.statistics;

import pri.pojo.SaleData;
import pri.pojo.SalePercent;

import java.util.List;

public interface StatisticsService {

    List<SalePercent> getSalePercents(List<SaleData> saleDatas, int totalSaleNum);
}
