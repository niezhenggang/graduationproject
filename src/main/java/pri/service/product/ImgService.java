package pri.service.product;

import org.springframework.web.multipart.MultipartFile;

import java.util.List;


public interface ImgService {
    String saveImg(String path, MultipartFile imgFile);

    List<String> saveImgs(String basePath, List<MultipartFile> imgFiles);
}
