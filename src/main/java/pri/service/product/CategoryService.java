package pri.service.product;

import pri.pojo.Category;
import pri.pojo.Page;

import java.util.List;

public interface CategoryService {

    Integer add(Category category);
    boolean delete(Integer id);
    boolean update(Category category);

    int countByExample(Category category);

    Category findById(Integer id,String userId);
    List<Category> findByExample(Category category, Page page);
}
