package pri.service.product;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pri.dao.CategoryMapper;
import pri.pojo.Category;
import pri.pojo.CategoryExample;
import pri.pojo.Page;

import java.util.List;

@Service
public class CategoryServiceImpl implements CategoryService{
    @Autowired
    CategoryMapper categoryMapper;

    @Override
    public Integer add(Category category) {
        return null;
    }

    @Override
    public boolean delete(Integer id) {
        return false;
    }

    @Override
    public boolean update(Category category) {
        return false;
    }

    @Override
    public int countByExample(Category category) {
        return 0;
    }

    @Override
    public Category findById(Integer id, String userId) {
        return null;
    }

    @Override
    public List<Category> findByExample(Category category, Page page) {
        CategoryExample example = new CategoryExample();
        example.createCriteria().andParentIdEqualTo(501000);

        if(category!=null){

        }

        if(page!=null){

        }

        return categoryMapper.selectByExample(example);
    }
}
