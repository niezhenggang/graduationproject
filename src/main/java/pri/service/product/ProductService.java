package pri.service.product;

import org.springframework.ui.Model;
import org.springframework.web.multipart.MultipartFile;
import pri.pojo.*;

import java.util.List;

public interface ProductService {

    Integer add(ProductInfo product);
    boolean delete(Integer id);
    boolean update(Product product);

    int allBrow();

    int countByExample(ProductInfo product);

    ProductInfo findById(Integer id,String userId);
    List<ProductInfo> findByExample(ProductInfo product, Page page);

    Integer collection(Integer productId,String userId);

    Integer cancelCollection(Integer productId, String userId);

    List<ProductInfo> getCollections(String userId,int number);

    List<Category> hotCategory();

    ProductInfo transQuery(ProductInfo old,ProductInfo newo);

    List<ProductInfo> mastLook();

    void addHeat(Integer productId,int number);

    Integer addComment(ProductComment productComment, String userId);


}
