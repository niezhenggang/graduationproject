package pri.service.order;

import pri.pojo.RecAddress;
import pri.pojo.SaleData;

import java.util.Date;
import java.util.List;
import java.util.Map;

public interface OrderService {
    List<SaleData> getSale(Date startTime, Date endTime);

    int getOrderNum(Date startTime, Date endTime);

    List<Map> getStatusOrder(String page, String limit, String status);

    RecAddress getAddressByOrderId(String oid);

    boolean sendProduct(String oid, String expressCom, String expressNum);

    void changeOrderStatus(String oid, String status);
}
