package pri.service.login;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pri.dao.AdminMapper;
import pri.pojo.Admin;
import pri.pojo.AdminExample;
import java.util.List;

@Service
public class LoginServiceImpl implements LoginService{
    @Autowired
    private AdminMapper adminMapper;



    @Override
    public Admin selectByAdminNameAndPassword(Admin admin) {
        AdminExample adminExample=new AdminExample();
        adminExample.createCriteria().andAdminNameEqualTo(admin.getAdminName()).andPasswordEqualTo(admin.getPassword());
//                (admin.getAdminName()).andPasswordEqualTo(admin.getPassword());
        List<Admin> admins= adminMapper.selectByExample(adminExample);
        return (admins.isEmpty()||admins==null)?null:admins.get(0);
//        return admins.isEmpty()?null:admins.get(0);
    }


}
