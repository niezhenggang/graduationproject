package pri.service.user;

import pri.pojo.User;

public interface UserService {

    User getUserById(String userid);
}
