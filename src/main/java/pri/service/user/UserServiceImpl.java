package pri.service.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pri.dao.UserMapper;
import pri.pojo.User;
import pri.pojo.UserExample;

import java.util.List;

@Service
public class UserServiceImpl implements UserService{
    @Autowired
    UserMapper userMapper;

    @Override
    public User getUserById(String userid) {
        UserExample userExample = new UserExample();
        userExample.createCriteria().andUserIdEqualTo(userid);
        List<User> users = userMapper.selectByExample(userExample);
        if (users == null || users.isEmpty()){
            User user = new User();
            user.setNickName("帅不谈");
            return user;
        }else {
            return users.get(0);
        }
    }
}
