<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <title>Login Page</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
    <!-- Bootstrap Core CSS -->
    <link href="${pageContext.request.contextPath}/assets/css/bootstrap.css" rel='stylesheet' type='text/css' />
    <!-- Custom CSS -->
    <link href="${pageContext.request.contextPath}/assets/css/style.css" rel='stylesheet' type='text/css' />
    <!-- font CSS -->
    <!-- font-awesome icons -->
    <link href="${pageContext.request.contextPath}/assets/css/font-awesome.css" rel="stylesheet">
    <!-- //font-awesome icons -->
    <!-- js-->
    <script src="${pageContext.request.contextPath}/assets/js/jquery-1.11.1.min.js"></script>
    <script src="${pageContext.request.contextPath}/assets/js/modernizr.custom.js"></script>
    <!--webfonts-->
    <link href='http://fonts.useso.com/css?family=Roboto+Condensed:400,300,300italic,400italic,700,700italic' rel='stylesheet' type='text/css'>
    <!--//webfonts-->
    <!--animate-->
    <link href="${pageContext.request.contextPath}/assets/css/animate.css" rel="stylesheet" type="text/css" media="all">
    <script src="${pageContext.request.contextPath}/assets/js/wow.min.js"></script>
    <script>
        new WOW().init();
    </script>
    <!--//end-animate-->
    <!-- Metis Menu -->
    <script src="${pageContext.request.contextPath}/assets/js/metisMenu.min.js"></script>
    <script src="${pageContext.request.contextPath}/assets/js/custom.js"></script>
    <link href="${pageContext.request.contextPath}/assets/css/custom.css" rel="stylesheet">
    <!--//Metis Menu -->
</head>
<body>
<div class="main-content">
    <!-- main content start-->
    <div id="page-wrapper" style="min-height: 740px;padding-left: 24px;margin-left: 0px;padding-bottom: 104px;">
        <div class="main-page login-page ">
            <h3 class="title1">SignIn Page</h3>
            <div class="widget-shadow">
                <div class="login-top">
                    <h4>欢迎登录SBT数码商城 </h4>
                </div>
                <div class="login-body">

                        <input type="text" class="user" name="adminName" placeholder="请输入您的账号" required="" id ="adminName">
                        <input type="password" name="password" class="lock" placeholder="请输入您的密码" id = "password">

                        <label class="checkbox"><input type="checkbox"  id="rememberMe"  name="rememberMe" checked=""><i></i>Remember me</label>
                        <div class="clearfix"> </div>

                        <input type="submit" name="Sign In" onclick="login()" value="登录">


                </div>
            </div>


        </div>
    </div>
</div>

<!-- Classie -->
<script src="${pageContext.request.contextPath}/assets/js/classie.js"></script>
<!--scrolling js-->
<script src="${pageContext.request.contextPath}/assets/js/jquery.nicescroll.js"></script>
<script src="${pageContext.request.contextPath}/assets/js/scripts.js"></script><div id="ascrail2000" class="nicescroll-rails" style="width: 6px; z-index: 1000; background: rgb(66, 79, 99); cursor: default; position: fixed; top: 0px; height: 100%; right: 0px; display: none;"><div style="position: relative; top: 0px; float: right; width: 6px; height: 0px; background-color: rgb(242, 179, 63); border: 0px; background-clip: padding-box; border-radius: 10px;"></div></div><div id="ascrail2000-hr" class="nicescroll-rails" style="height: 6px; z-index: 1000; background: rgb(66, 79, 99); position: fixed; left: 0px; width: 100%; bottom: 0px; cursor: default; display: none;"><div style="position: relative; top: 0px; height: 6px; width: 0px; background-color: rgb(242, 179, 63); border: 0px; background-clip: padding-box; border-radius: 10px;"></div></div>
<!--//scrolling js-->
<!-- Bootstrap Core JavaScript -->
<script src="${pageContext.request.contextPath}/assets/js/bootstrap.js"> </script>
</body>


<script type="text/javascript">
    function login() {
// 	编写函数,在按键升起时触发,监测cookie中是否存在该用户名的key,如果有,则把value赋值给密码框
        var adminName = $("#adminName").val();
        var password = $("#password").val();
        var rememberMe = $("input[name='rememberMe']").is(":checked");//是够勾选记住密码
        var obj = {"adminName": adminName, "password": password, "rememberMe":rememberMe} ;
        $.ajax({
                url: "${pageContext.request.contextPath}/login/doLogin",
                type: "POST",
                async: true,
                data: {data:JSON.stringify(obj)},
                dataType: "json",
                success:function (data) {
                    console.log(data.code);
                    if (data.code=="0") {
                        alert("用户名或者密码错误");
                    }
                    else {
                        window.location.href="${pageContext.request.contextPath}/login/loginSuccess";
                    }
                },
                error:function () {
                    alert("网络异常");
                }
            }
        )

    };
</script>
</html>