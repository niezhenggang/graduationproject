<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<html>
<head>
    <base href="<%=basePath%>">
    <meta charset="UTF-8">
    <title>商品管理</title>

    <link rel="stylesheet" href="assets/layui/css/layui.css">
    <script src="assets/layui/layui.js"></script>

    <style>
        .layui-table-cell{
            height:auto !important;
        }
        /*提示框*/
        .tipsbox{
            position: fixed;
            top: 40%;
            left: 50%;
            z-index: 10000;
            min-width: 200px;
            min-height: 40px;
            max-width: 500px;
            border-radius: 5px;
            text-align: center;
            padding: 10px;
            color: white;
            background-color: red;
            opacity: 0.6;
            display: none;
        }
    </style>

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="Novus Admin Panel Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template,
SmartPhone Compatible web template, free WebDesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
    <!-- Bootstrap Core CSS -->
    <link href="assets/css/bootstrap.css" rel='stylesheet' type='text/css' />
    <!-- Custom CSS -->
    <link href="assets/css/style.css" rel='stylesheet' type='text/css' />
    <!-- font CSS -->
    <!-- font-awesome icons -->
    <link href="assets/css/font-awesome.css" rel="stylesheet">
    <!-- //font-awesome icons -->
    <!-- js-->
    <script src="assets/js/jquery-1.11.1.min.js"></script>
    <script src="assets/js/modernizr.custom.js"></script>
    <!--webfonts-->
    <link href='http://fonts.useso.com/css?family=Roboto+Condensed:400,300,300italic,400italic,700,700italic' rel='stylesheet' type='text/css'>
    <!--//webfonts-->
    <!--animate-->
    <link href="assets/css/animate.css" rel="stylesheet" type="text/css" media="all">
    <script src="assets/js/wow.min.js"></script>
    <script>
        new WOW().init();
    </script>
    <!--//end-animate-->
    <!-- chart -->
    <script src="assets/js/Chart.js"></script>
    <!-- //chart -->
    <!--Calender-->
    <link rel="stylesheet" href="assets/css/clndr.css" type="text/css" />
    <script src="assets/js/underscore-min.js" type="text/javascript"></script>
    <script src= "assets/js/moment-2.2.1.js" type="text/javascript"></script>
    <script src="assets/js/clndr.js" type="text/javascript"></script>
    <script src="assets/js/site.js" type="text/javascript"></script>
    <!--End Calender-->
    <!-- Metis Menu -->
    <script src="assets/js/metisMenu.min.js"></script>
    <script src="assets/js/custom.js"></script>
    <link href="assets/css/custom.css" rel="stylesheet">
    <!--//Metis Menu -->

</head>
<body>
<%--<%@include file="/view/back/head.jsp"%>--%>
<div class="main-content">
    <!--left-fixed -navigation-->
    <div class=" sidebar" role="navigation">
        <div class="navbar-collapse">
            <nav class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-left" id="cbp-spmenu-s1">
                <ul class="nav" id="side-menu">
                    <li>
                        <a href="index.html" class="active"><i class="fa fa-home nav_icon"></i>后台总览</a>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-cogs nav_icon"></i>用户管理<span class="nav-badge">12</span> <span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level collapse">
                            <li>
                                <a href="grids.html">修改密码</a>
                            </li>
                            <li>
                                <a href="media.html">修改个人信息</a>
                            </li>
                        </ul>
                        <!-- /nav-second-level -->
                    </li>
                    <li class="">
                        <a href="#"><i class="fa fa-book nav_icon"></i>商品管理 <span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level collapse">
                            <li>
                                <a href="general.html">商品分类<span class="nav-badge-btm">08</span></a>
                            </li>
                            <li>
                                <a href="typography.html">图片展示</a>
                            </li>
                            <li>
                                <a href="general.html">商品信息</a>
                            </li>
                            <li>
                                <a href="general.html">上下架</a>
                            </li><li>
                            <a href="general.html">评论区</a>
                        </li>
                        </ul>
                        <!-- /nav-second-level -->
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-th-large nav_icon"></i>订单 <span class="nav-badge-btm">08</span></a>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-envelope nav_icon"></i>购物车<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level collapse">
                            <li>
                                <a href="#">购物车清单<span class="nav-badge-btm">05</span></a>
                            </li>
                            <li>
                                <a href="#">支付</a>
                            </li>
                        </ul>
                        <!-- //nav-second-level -->
                    </li>
                    <li>
                        <a href="tables.html"><i class="fa fa-table nav_icon"></i>优惠券管理 <span class="nav-badge">05</span></a>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-check-square-o nav_icon"></i>促销商品<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level collapse">
                            <li>
                                <a href="forms.html">手机 <span class="nav-badge-btm">07</span></a>
                            </li>
                            <li>
                                <a href="validation.html">电脑</a>
                            </li>
                        </ul>
                        <!-- //nav-second-level -->
                    </li>

                </ul>
                <!-- //sidebar-collapse -->
            </nav>
        </div>
    </div>
    <!--left-fixed -navigation-->
    <!-- header-starts -->
    <div class="sticky-header header-section ">
        <div class="header-left">
            <!--toggle button start-->
            <button id="showLeftPush"><i class="fa fa-bars"></i></button>
            <!--toggle button end-->
            <!--logo -->
            <div class="logo">
                <a href="index.html">
                    <h1>S-B-T</h1>
                    <span>后台管理</span>
                </a>
            </div>
            <!--//logo-->
            <!--search-box-->
            <div class="search-box">
                <form class="input">
                    <input class="sb-search-input input__field--madoka" placeholder="搜索..." type="search" id="input-31" />
                    <label class="input__label" for="input-31">
                        <svg class="graphic" width="100%" height="100%" viewBox="0 0 404 77" preserveAspectRatio="none">
                            <path d="m0,0l404,0l0,77l-404,0l0,-77z"/>
                        </svg>
                    </label>
                </form>
            </div><!--//end-search-box-->
            <div class="clearfix"> </div>
        </div>
        <div class="header-right">
            <div class="profile_details_left"><!--notifications of menu start -->
                <ul class="nofitications-dropdown">
                    <li class="dropdown head-dpdn">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-envelope"></i><span class="badge">3</span></a>
                        <ul class="dropdown-menu">
                            <li>
                                <div class="notification_header">
                                    <h3>你收到了三条信息</h3>
                                </div>
                            </li>
                            <li><a href="#">
                                <div class="user_img"><img src="${pageContext.request.contextPath}/assets/images/1.png" alt=""></div>
                                <div class="notification_desc">
                                    <p>Lorem ipsum dolor amet</p>
                                    <p><span>1 hour ago</span></p>
                                </div>
                                <div class="clearfix"></div>
                            </a></li>
                            <li class="odd"><a href="#">
                                <div class="user_img"><img src="${pageContext.request.contextPath}/assets/images/2.png" alt=""></div>
                                <div class="notification_desc">
                                    <p>Lorem ipsum dolor amet </p>
                                    <p><span>1 hour ago</span></p>
                                </div>
                                <div class="clearfix"></div>
                            </a></li>
                            <li><a href="#">
                                <div class="user_img"><img src="${pageContext.request.contextPath}/assets/images/3.png" alt=""></div>
                                <div class="notification_desc">
                                    <p>Lorem ipsum dolor amet </p>
                                    <p><span>1 hour ago</span></p>
                                </div>
                                <div class="clearfix"></div>
                            </a></li>
                            <li>
                                <div class="notification_bottom">
                                    <a href="#">查看所有信息</a>
                                </div>
                            </li>
                        </ul>
                    </li>
                    <li class="dropdown head-dpdn">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-bell"></i><span class="badge blue">3</span></a>
                        <ul class="dropdown-menu">
                            <li>
                                <div class="notification_header">
                                    <h3>你收到了三条通知</h3>
                                </div>
                            </li>
                            <li><a href="#">
                                <div class="user_img"><img src="${pageContext.request.contextPath}/assets/images/2.png" alt=""></div>
                                <div class="notification_desc">
                                    <p>Lorem ipsum dolor amet</p>
                                    <p><span>1 hour ago</span></p>
                                </div>
                                <div class="clearfix"></div>
                            </a></li>
                            <li class="odd"><a href="#">
                                <div class="user_img"><img src="${pageContext.request.contextPath}/assets/images/1.png" alt=""></div>
                                <div class="notification_desc">
                                    <p>Lorem ipsum dolor amet </p>
                                    <p><span>1 hour ago</span></p>
                                </div>
                                <div class="clearfix"></div>
                            </a></li>
                            <li><a href="#">
                                <div class="user_img"><img src="${pageContext.request.contextPath}/assets/images/3.png" alt=""></div>
                                <div class="notification_desc">
                                    <p>Lorem ipsum dolor amet </p>
                                    <p><span>1 hour ago</span></p>
                                </div>
                                <div class="clearfix"></div>
                            </a></li>
                            <li>
                                <div class="notification_bottom">
                                    <a href="#">查看所有通知</a>
                                </div>
                            </li>
                        </ul>
                    </li>
                    <li class="dropdown head-dpdn">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-tasks"></i><span class="badge blue1">15</span></a>
                        <ul class="dropdown-menu">
                            <li>
                                <div class="notification_header">
                                    <h3>你有八个挂起的任务</h3>
                                </div>
                            </li>
                            <li><a href="#">
                                <div class="task-info">
                                    <span class="task-desc">Database update</span><span class="percentage">40%</span>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="progress progress-striped active">
                                    <div class="bar yellow" style="width:40%;"></div>
                                </div>
                            </a></li>
                            <li><a href="#">
                                <div class="task-info">
                                    <span class="task-desc">Dashboard done</span><span class="percentage">90%</span>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="progress progress-striped active">
                                    <div class="bar green" style="width:90%;"></div>
                                </div>
                            </a></li>
                            <li><a href="#">
                                <div class="task-info">
                                    <span class="task-desc">Mobile App</span><span class="percentage">33%</span>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="progress progress-striped active">
                                    <div class="bar red" style="width: 33%;"></div>
                                </div>
                            </a></li>
                            <li><a href="#">
                                <div class="task-info">
                                    <span class="task-desc">Issues fixed</span><span class="percentage">80%</span>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="progress progress-striped active">
                                    <div class="bar  blue" style="width: 80%;"></div>
                                </div>
                            </a></li>
                            <li>
                                <div class="notification_bottom">
                                    <a href="#">查看所有任务</a>
                                </div>
                            </li>
                        </ul>
                    </li>
                </ul>
                <div class="clearfix"> </div>
            </div>
            <!--notification menu end -->
            <div class="profile_details">
                <ul>
                    <li class="dropdown profile_details_drop">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                            <div class="profile_img">
                                <span class="prfil-img"><img src="assets/images/a.png" alt=""> </span>
                                <div class="user-name">
                                    <p>周大帅</p>
                                    <span>Administrator</span>
                                </div>
                                <i class="fa fa-angle-down lnr"></i>
                                <i class="fa fa-angle-up lnr"></i>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                        <ul class="dropdown-menu drp-mnu">
                            <li> <a href="#"><i class="fa fa-cog"></i> Settings</a> </li>
                            <li> <a href="#"><i class="fa fa-user"></i> Profile</a> </li>
                            <li> <a href="#"><i class="fa fa-sign-out"></i> Logout</a> </li>
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="clearfix"> </div>
        </div>
        <div class="clearfix"> </div>
    </div>
    <!-- //header-ends -->
    <!-- main content start-->

    <div id="page-wrapper">
        <div id="container" style="margin-left: 320px">

            <table id="product-list" class="layui-table" lay-data="{height:700, url:'product/all', page:true, id:'product-list'}" lay-filter="test">

                <thead>

                <tr>
                    <th lay-data="{field:'productId', width:120, sort: true}">ID</th>
                    <th lay-data="{field:'name', width:300, sort: true}">商品名</th>
                    <th lay-data="{field:'img', width:100,templet:'#img'}">商品图片</th>
                    <th lay-data="{field:'price', width:80, sort: true}">原价格</th>
                    <th lay-data="{field:'concessionalPrice', width:80, sort: true}">优惠价</th>
                    <th lay-data="{field:'stock', width:80}">库存</th>
                    <th lay-data="{field:'categoryName', width:100,templet:'#category', sort: true}">分类</th>
                    <th lay-data="{field:'color', width:100}">颜色</th>
                    <th lay-data="{field:'stateName', width:80}">状态</th>
                    <th lay-data="{field:'createTime', width:120, sort: true}">创建时间</th>
                    <th lay-data="{field:'updateTime', width:120, sort: true}">更新时间</th>
                    <th lay-data="{field:'right', width:200,align:'center',toolbar:'#toolbar'}"></th>
                </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
    <%@include file="/view/back/foot.jsp"%>


<script type="text/html" id="img">
        <img src="{{(d.images)[0].url}}" style="max-width: 70px;max-height: 70px" class="" alt=""/>
    </script>
<script type="text/html" id="category">
        <span  class="" categoryId="{{d.categoryId}}">{{d.categoryName}}</span>
    </script>
<script type="text/html" id="toolbar">
<%--        <a class="layui-btn layui-btn-xs" lay-event="detail">查看</a>--%>

        {{#  if(d.stateName == '未上架'){ }}
            <a class="layui-btn layui-btn-xs" lay-event="update" onclick="update('{{d.productId}}')">上架</a>
        {{#  } else { }}
            <a class="layui-btn layui-btn-xs" lay-event="update" onclick="down('{{d.productId}}')">下架</a>
        {{#  } }}

        <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="delete" onclick="del('{{d.productId}}')">删除</a>
    </script>


    <script src="https://cdn.bootcss.com/jquery/1.9.1/jquery.min.js"></script>
</body>
</html>

<script>

    function update(productId){
        $.ajax({
            url:"product/update?stateId=100&productId="+productId,
            type:"post",
            success:function(res){
                if(res==true){
                    tipsbox("上架成功！","green",1500);
                    setTimeout(function () {
                        window.location.reload();
                    },2000);
                }else{
                    tipsbox("上架失败！","red",1500);
                }
            },
            error:function(){
                tipsbox("网络异常，请稍后再试！","red",1500);
            }
        });
    };

    function down(productId){
        $.ajax({
            url:"product/update?stateId=1000&productId="+productId,
            type:"post",
            success:function(res){
                if(res==true){
                    tipsbox("下架成功！","green",1500);
                    setTimeout(function () {
                        window.location.reload();
                    },2000);
                }else{
                    tipsbox("下架失败！","red",1500);
                }
            },
            error:function(){
                tipsbox("网络异常，请稍后再试！","red",1500);
            }
        });
    };

    function del(productId){
        $.ajax({
            url:"product/delete?id="+productId,
            type:"post",
            success:function(res){
                if(res==true){
                    tipsbox("删除成功！","green",1500);
                    setTimeout(function () {
                        window.location.reload();
                    },2000);
                }else{
                    tipsbox("删除失败！","red",1500);
                }
            },
            error:function(){
                tipsbox("网络异常，请稍后再试！","red",1500);
            }
        });
    };

    layui.use('table',function () {
        var table = layui.table;

        /* table.render({
             elem:'#product-list',
             height:500,
             url:'product/list',
             page:true,
         });*/


        //监听事件
        table.on('toolbar(test)', function(obj){
            console.log(obj);
            var checkStatus = table.checkStatus(obj.config.id);
            switch(obj.event){
                case 'detail':
                    console.log(obj);
                    layer.msg('查看');
                    break;
                case 'update':
                    console.log(obj);
                    layer.msg('上架');
                    break;
                case 'delete':
                    console.log(obj);
                    layer.msg('删除');
                    break;
            };
        });
    });

    function tipsbox(msg,bgcolor,time) {
        // alert("收藏成功");
        $("body").append("<div id='tips' class='tipsbox'>" + msg + "</div>");
        var tips = $("#tips");
        tips.css("margin-left","-"+tips.width()/2+"px");
        if(bgcolor!=null){
            tips.css("background-color",bgcolor);
        }
        if(time == null){
            time = 2000;
        }
        tips.fadeIn("slow");
        setTimeout(function () {
            tips.fadeOut("slow");
            setTimeout(function () {
                tips.remove();
            },1000);
        },time)

    }

</script>
<script src="assets/js/classie.js"></script>
<script>
    var menuLeft = document.getElementById( 'cbp-spmenu-s1' ),
        showLeftPush = document.getElementById( 'showLeftPush' ),
        body = document.body;

    showLeftPush.onclick = function() {
        classie.toggle( this, 'active' );
        classie.toggle( body, 'cbp-spmenu-push-toright' );
        classie.toggle( menuLeft, 'cbp-spmenu-open' );
        disableOther( 'showLeftPush' );
    };


    function disableOther( button ) {
        if( button !== 'showLeftPush' ) {
            classie.toggle( showLeftPush, 'disabled' );
        }
    }
</script>
<!--scrolling js-->
<script src="assets/js/jquery.nicescroll.js"></script>
<script src="assets/js/scripts.js"></script>
<!--//scrolling js-->
<!-- Bootstrap Core JavaScript -->
<script src="assets/js/bootstrap.js"> </script>
