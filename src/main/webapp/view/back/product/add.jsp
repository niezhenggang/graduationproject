<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<html>
<head>
    <base href="<%=basePath%>">
    <meta charset="UTF-8">
    <title>添加商品</title>
    <link rel="stylesheet" href="https://res.wx.qq.com/open/libs/weui/2.0.0/weui.min.css">
    <link rel="stylesheet" href="assets/layui/css/layui.css">
    <script src="assets/layui/layui.js"></script>


    <style>
        /*提示框*/
        .tipsbox{
            position: fixed;
            top: 40%;
            left: 50%;
            z-index: 10000;
            min-width: 200px;
            min-height: 40px;
            max-width: 500px;
            border-radius: 5px;
            text-align: center;
            padding: 10px;
            color: white;
            background-color: red;
            opacity: 0.6;
            display: none;
        }

        .btn-cancelCollection{
            background-color: #f5740a;
            color: #fff;
        }
    </style>
</head>
<body>
<div style="max-width: 800px;margin: auto;padding-top:50px;">
    <form class="layui-form" action="product/add" method="post" modelAttribute="ProductInfo">
        <div class="layui-form-item">
            <label class="layui-form-label">商品名</label>
            <div class="layui-input-inline">
                <input type="text" name="name" required  lay-verify="required" placeholder="请输入商品名" autocomplete="off" class="layui-input col-lg-4">
            </div>
            <div class="layui-form-mid layui-word-aux">商品名应尽量言简意赅</div>
        </div>
        <div class="layui-form-item">
            <button type="button" class="layui-form-label layui-btn" id="addImg">上传图片</button>
            <input type="file" style="display: none" id="imgFile">
            <div class="layui-input-block">
                <div class="layui-upload">
                    <blockquote class="layui-elem-quote layui-quote-nm" style="">
                        预览图：
                        <div class="layui-upload-list" id="demo2"></div>
                    </blockquote>
                </div>
            </div>
        </div>

        <div class="layui-form-item">
            <label class="layui-form-label">价格</label>
            <div class="layui-input-inline">
                <input type="number" name="price" required lay-verify="required" placeholder="请输入价格" autocomplete="off" class="layui-input">
            </div>
            <div class="layui-form-mid layui-word-aux">价格要合理</div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">优惠后价格</label>
            <div class="layui-input-inline">
                <input type="number" name="concessionalPrice"  placeholder="请输入优惠价格"   autocomplete="off" class="layui-input">
            </div>
            <div class="layui-form-mid layui-word-aux">如不需对商品价格进行优惠，不输入</div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">库存</label>
            <div class="layui-input-inline">
                <input type="number" name="stock" required lay-verify="required" placeholder="请输入库存" autocomplete="off" class="layui-input">
            </div>
            <div class="layui-form-mid layui-word-aux">库存要合理</div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">分类</label>
            <div class="layui-input-inline">
                <select name="categoryId" lay-verify="required">
                    <option value="" selected></option>
                    <c:if test="${category!=null}">
                        <c:forEach items="${category}" var="category">
                            <option value="${category.id}">${category.name}</option>
                        </c:forEach>
                    </c:if>

                </select>
            </div>
        </div>

        <div class="layui-form-item">
            <label class="layui-form-label">颜色</label>
            <div class="layui-input-inline">
                <input type="text" name="color" required lay-verify="required" placeholder="请输入颜色名" autocomplete="off" class="layui-input">
            </div>
            <div class="layui-form-mid layui-word-aux">颜色要合理</div>
        </div>

        <div class="layui-form-item">
            <label class="layui-form-label">立即上架</label>
            <div class="layui-input-block">
                <input type="checkbox" name="stateName" lay-skin="switch">
            </div>
        </div>
        <div class="layui-form-item layui-form-text">
            <label class="layui-form-label">输入商品详情</label>
            <div class="layui-input-block">
                <textarea name="detail" required placeholder="请输入内容" class="layui-textarea"></textarea>
            </div>
        </div>


        <div class="layui-form-item">
            <div class="layui-input-inline " style="margin: auto;">
                <button class="layui-btn" lay-submit lay-filter="formDemo">立即提交</button>
                <button type="reset" class="layui-btn layui-btn-primary">重置</button>
            </div>
        </div>
    </form>
</div>


<script src="https://cdn.bootcss.com/jquery/1.9.1/jquery.min.js"></script>
<script>
    //Demo
    layui.use('form', function(){
        var form = layui.form;

        //监听提交
        form.on('submit(formDemo)', function(data){

            // layer.msg(JSON.stringify(data.field));


            return true;
        });
    });


    /*layui.use('upload', function(){
        var $ = layui.jquery
            ,upload = layui.upload;

        //多图片上传
        upload.render({
            elem: '#test2'
            ,url: 'product/addImg'
            ,multiple: true
            ,before: function(obj){
                //预读本地文件示例，不支持ie8
                obj.preview(function(index, file, result){
                    $('#demo2').append('<img src="'+ result +'" alt="'+ file.name +'" class="layui-upload-img" style="width: 70px;height: 70px;">')
                });
            }
            ,done: function(res){
                console.log(res);
                //上传完毕
                if(res.data.src != null && res.data.src != ""){
                    tipsbox("上传成功","green",1500);
                    $('#demo2').append("<input type='text' hidden='hidden' name='imgs' value='"+res.data.src+"'>");
                }else{
                    tipsbox("上传失败，请重试","red",1500);
                }
            }
        });

    });*/

    $("#addImg").click(function () {
        $("#imgFile").click();
    });

    $('#imgFile').change(function(){
        var image=this.files[0];
        var formData = new FormData();
        formData.append('smfile',image);
        $.ajax({
            url: 'https://sm.ms/api/upload',
            type: 'POST',
            data:formData,
            success: function(res){
                console.log("````````成功`````````````")
                console.log(res);
                if(res.code=="success") {
                    tipsbox("上传成功","green",1500);
                    $('#demo2').append('<img src="' + res.data.url + '" alt="' + res.data.filename + '" class="layui-upload-img" style="width: 70px;height: 70px;">')
                    $('#demo2').append("<input type='text' hidden='hidden' name='imgs' value='"+res.data.url+"'>");
                }
            },
            error: function(data){
                console.log("````````失败`````````````")
                console.log(data);
                tipsbox("上传失败，请重试","red",1500);
            },
            cache: false,
            contentType: false,
            processData: false
        });
    });




    function tipsbox(msg,bgcolor,time) {
        // alert("收藏成功");
        $("body").append("<div id='tips' class='tipsbox'>" + msg + "</div>");
        var tips = $("#tips");
        tips.css("margin-left","-"+tips.width()/2+"px");
        if(bgcolor!=null){
            tips.css("background-color",bgcolor);
        }
        if(time == null){
            time = 2000;
        }
        tips.fadeIn("slow");
        setTimeout(function () {
            tips.fadeOut("slow");
            setTimeout(function () {
                tips.remove();
            },1000);
        },time)

    }

</script>

</body>
</html>
