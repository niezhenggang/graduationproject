<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>zhashabi</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
    <!-- Bootstrap Core CSS -->
    <link href="${pageContext.request.contextPath}/assets/css/bootstrap.css" rel='stylesheet' type='text/css' />
    <!-- Custom CSS -->
    <link href="${pageContext.request.contextPath}/assets/css/style.css" rel='stylesheet' type='text/css' />
    <!-- font CSS -->
    <!-- font-awesome icons -->
    <link href="${pageContext.request.contextPath}/assets/css/font-awesome.css" rel="stylesheet">
    <!-- //font-awesome icons -->
    <!-- js-->
    <script src="${pageContext.request.contextPath}/assets/js/jquery-1.11.1.min.js"></script>
    <script src="${pageContext.request.contextPath}/assets/js/modernizr.custom.js"></script>
    <!--webfonts-->
    <link href='http://fonts.useso.com/css?family=Roboto+Condensed:400,300,300italic,400italic,700,700italic' rel='stylesheet' type='text/css'>
    <!--//webfonts-->
    <!--animate-->
    <link href="${pageContext.request.contextPath}/assets/css/animate.css" rel="stylesheet" type="text/css" media="all">
    <script src="${pageContext.request.contextPath}/assets/js/wow.min.js"></script>
    <script>
        new WOW().init();
    </script>
    <!--//end-animate-->
    <!-- chart -->
    <script src="${pageContext.request.contextPath}/assets/js/Chart.js"></script>
    <!-- //chart -->
    <!--Calender-->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/clndr.css" type="text/css" />
    <script src="${pageContext.request.contextPath}/assets/js/underscore-min.js" type="text/javascript"></script>
    <script src= "${pageContext.request.contextPath}/assets/js/moment-2.2.1.js" type="text/javascript"></script>
    <script src="${pageContext.request.contextPath}/assets/js/clndr.js" type="text/javascript"></script>
    <script src="${pageContext.request.contextPath}/assets/js/site.js" type="text/javascript"></script>
    <!--End Calender-->
    <!-- Metis Menu -->
    <script src="${pageContext.request.contextPath}/assets/js/metisMenu.min.js"></script>
    <script src="${pageContext.request.contextPath}/assets/js/custom.js"></script>
    <link href="${pageContext.request.contextPath}/assets/css/custom.css" rel="stylesheet">
    <!--//Metis Menu -->
    <!--layui-->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/layui/css/layui.css">
    <script src="${pageContext.request.contextPath}/assets/layui/layui.js"></script>
    <style>
        .layui-table-cell{
            height:auto !important;
        }
        /*提示框*/
        .tipsbox{
            position: fixed;
            top: 40%;
            left: 50%;
            z-index: 10000;
            min-width: 200px;
            min-height: 40px;
            max-width: 500px;
            border-radius: 5px;
            text-align: center;
            padding: 10px;
            color: white;
            background-color: red;
            opacity: 0.6;
            display: none;
        }
    </style>
</head>
<body class="cbp-spmenu-push">
<%@ include file="../head.jsp"%>








<div id="page-wrapper">

    <div id="container" style="margin-left: 40px">
        <table id="product-list" class="layui-table" lay-data="{height:700, url:'${pageContext.request.contextPath}/product/all', page:true, id:'product-list'}" lay-filter="test">
            <thead>
            <tr>
                <th lay-data="{field:'productId', width:120, sort: true}">ID</th>
                <th lay-data="{field:'name', width:300, sort: true}">商品名</th>
                <th lay-data="{field:'img', width:100,templet:'#img'}">商品图片</th>
                <th lay-data="{field:'price', width:80, sort: true}">原价格</th>
                <th lay-data="{field:'concessionalPrice', width:80, sort: true}">优惠价</th>
                <th lay-data="{field:'stock', width:80}">库存</th>
                <th lay-data="{field:'categoryName', width:100,templet:'#category', sort: true}">分类</th>
                <th lay-data="{field:'color', width:100}">颜色</th>
                <th lay-data="{field:'stateName', width:80}">状态</th>
                <th lay-data="{field:'createTime', width:120, sort: true}">创建时间</th>
                <th lay-data="{field:'updateTime', width:120, sort: true}">更新时间</th>
                <th lay-data="{field:'right', width:200,align:'center',toolbar:'#toolbar'}"></th>
            </tr>
            </thead>
        </table>
    </div>
</div>













<%@ include file="../foot.jsp"%>
<!--//footer--><!-- Classie -->
<script src="${pageContext.request.contextPath}/assets/js/classie.js"></script>
<script>
    var menuLeft = document.getElementById( 'cbp-spmenu-s1' ),
        showLeftPush = document.getElementById( 'showLeftPush' ),
        body = document.body;

    showLeftPush.onclick = function() {
        classie.toggle( this, 'active' );
        classie.toggle( body, 'cbp-spmenu-push-toright' );
        classie.toggle( menuLeft, 'cbp-spmenu-open' );
        disableOther( 'showLeftPush' );
    };


    function disableOther( button ) {
        if( button !== 'showLeftPush' ) {
            classie.toggle( showLeftPush, 'disabled' );
        }
    }
</script>
<!--scrolling js-->
<script src="${pageContext.request.contextPath}/assets/js/jquery.nicescroll.js"></script>
<script src="${pageContext.request.contextPath}/assets/js/scripts.js"></script>
<!--//scrolling js-->
<!-- Bootstrap Core JavaScript -->
<script src="${pageContext.request.contextPath}/assets/js/bootstrap.js"> </script>


//my
<script type="text/html" id="img">
    <img src="{{(d.images)[0].url}}" style="max-width: 70px;max-height: 70px" class="" alt=""/>
</script>
<script type="text/html" id="category">
    <span  class="" categoryId="{{d.categoryId}}">{{d.categoryName}}</span>
</script>
<script type="text/html" id="toolbar">
    <%--        <a class="layui-btn layui-btn-xs" lay-event="detail">查看</a>--%>

    {{#  if(d.stateName == '未上架'){ }}
    <a class="layui-btn layui-btn" lay-event="update" onclick="update('{{d.productId}}')">上架</a>
    {{#  } else { }}
    <a class="layui-btn layui-btn" lay-event="update" onclick="down('{{d.productId}}')">下架</a>
    {{#  } }}

    <a class="layui-btn layui-btn-danger layui-btn" lay-event="delete" onclick="del('{{d.productId}}')">删除</a>
</script>
<script>

    function update(productId){
        $.ajax({
            url:"${pageContext.request.contextPath}/product/update?stateId=100&productId="+productId,
            type:"post",
            success:function(res){
                if(res==true){
                    tipsbox("上架成功！","green",1500);
                    setTimeout(function () {
                        window.location.reload();
                    },2000);
                }else{
                    tipsbox("上架失败！","red",1500);
                }
            },
            error:function(){
                tipsbox("网络异常，请稍后再试！","red",1500);
            }
        });
    };

    function down(productId){
        $.ajax({
            url:"${pageContext.request.contextPath}/product/update?stateId=1000&productId="+productId,
            type:"post",
            success:function(res){
                if(res==true){
                    tipsbox("下架成功！","green",1500);
                    setTimeout(function () {
                        window.location.reload();
                    },2000);
                }else{
                    tipsbox("下架失败！","red",1500);
                }
            },
            error:function(){
                tipsbox("网络异常，请稍后再试！","red",1500);
            }
        });
    };

    function del(productId){
        $.ajax({
            url:"${pageContext.request.contextPath}/product/delete?id="+productId,
            type:"post",
            success:function(res){
                if(res==true){
                    tipsbox("删除成功！","green",1500);
                    setTimeout(function () {
                        window.location.reload();
                    },2000);
                }else{
                    tipsbox("删除失败！","red",1500);
                }
            },
            error:function(){
                tipsbox("网络异常，请稍后再试！","red",1500);
            }
        });
    };

    layui.use('table',function () {
        var table = layui.table;

        /* table.render({
             elem:'#product-list',
             height:500,
             url:'product/list',
             page:true,
         });*/


        //监听事件
        table.on('toolbar(test)', function(obj){
            console.log(obj);
            var checkStatus = table.checkStatus(obj.config.id);
            switch(obj.event){
                case 'detail':
                    console.log(obj);
                    layer.msg('查看');
                    break;
                case 'update':
                    console.log(obj);
                    layer.msg('上架');
                    break;
                case 'delete':
                    console.log(obj);
                    layer.msg('删除');
                    break;
            };
        });
    });

    function tipsbox(msg,bgcolor,time) {
        // alert("收藏成功");
        $("body").append("<div id='tips' class='tipsbox'>" + msg + "</div>");
        var tips = $("#tips");
        tips.css("margin-left","-"+tips.width()/2+"px");
        if(bgcolor!=null){
            tips.css("background-color",bgcolor);
        }
        if(time == null){
            time = 2000;
        }
        tips.fadeIn("slow");
        setTimeout(function () {
            tips.fadeOut("slow");
            setTimeout(function () {
                tips.remove();
            },1000);
        },time)

    }

</script>

</body>
</html>
