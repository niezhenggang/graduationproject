
<%--
  Created by IntelliJ IDEA.
  User: 64124
  Date: 2019/7/11
  Time: 13:04
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<!DOCTYPE HTML>
<html>
<head>
    <title>Home</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="Novus Admin Panel Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template,
SmartPhone Compatible web template, free WebDesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
    <!-- Bootstrap Core CSS -->
    <link href="${pageContext.request.contextPath}/assets/css/bootstrap.css" rel='stylesheet' type='text/css' />
    <!-- Custom CSS -->
    <link href="${pageContext.request.contextPath}/assets/css/style.css" rel='stylesheet' type='text/css' />
    <!-- font CSS -->
    <!-- font-awesome icons -->
    <link href="${pageContext.request.contextPath}/assets/css/font-awesome.css" rel="stylesheet">
    <!-- //font-awesome icons -->
    <!-- js-->
    <script src="${pageContext.request.contextPath}/assets/js/jquery-1.11.1.min.js"></script>
    <script src="${pageContext.request.contextPath}/assets/js/modernizr.custom.js"></script>
    <!--webfonts-->
    <link href='http://fonts.useso.com/css?family=Roboto+Condensed:400,300,300italic,400italic,700,700italic' rel='stylesheet' type='text/css'>
    <!--//webfonts-->
    <!--animate-->
    <link href="${pageContext.request.contextPath}/assets/css/animate.css" rel="stylesheet" type="text/css" media="all">
    <script src="${pageContext.request.contextPath}/assets/js/wow.min.js"></script>
    <script>
        new WOW().init();
    </script>
    <!--//end-animate-->
    <!-- chart -->
    <script src="${pageContext.request.contextPath}/assets/js/Chart.js"></script>
    <!-- //chart -->
    <!--Calender-->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/clndr.css" type="text/css" />
    <script src="${pageContext.request.contextPath}/assets/js/underscore-min.js" type="text/javascript"></script>
    <script src= "${pageContext.request.contextPath}/assets/js/moment-2.2.1.js" type="text/javascript"></script>
    <script src="${pageContext.request.contextPath}/assets/js/clndr.js" type="text/javascript"></script>
    <script src="${pageContext.request.contextPath}/assets/js/site.js" type="text/javascript"></script>
    <!--End Calender-->
    <!-- Metis Menu -->
    <script src="${pageContext.request.contextPath}/assets/js/metisMenu.min.js"></script>
    <script src="${pageContext.request.contextPath}/assets/js/custom.js"></script>
    <link href="${pageContext.request.contextPath}/assets/css/custom.css" rel="stylesheet">
    <!--//Metis Menu -->

    <style>
        .sidebar ul li a {
        color: #FFFFFF;
        font-size: 0.9em;
    }</style>
</head>
<body class="cbp-spmenu-push">
<div class="main-content">
    <!--left-fixed -navigation-->
    <div class=" sidebar" role="navigation">
        <div class="navbar-collapse">
            <nav class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-left" id="cbp-spmenu-s1">
                <ul class="nav" id="side-menu">
                    <li>
                        <a href="/doStatistics" class="active"><i class="fa fa-home nav_icon"></i>后台总览</a>
                    </li>
                    <li>
                        <a href="/product/list"><i class="fa fa-th-large nav_icon"></i>商品列表管理 <span class="nav-badge-btm"></span></a>
                    </li>
                    <li>
                        <a href="/order/list"><i class="fa fa-th-large nav_icon"></i>订单管理<span class="nav-badge-btm"></span></a>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-table nav_icon"></i>优惠券管理 </a>
                    </li>
                        </ul>
                        <!-- /nav-second-level -->
                </ul>
                <!-- //sidebar-collapse -->
            </nav>
        </div>
    </div>
    <!--left-fixed -navigation-->
    <!-- header-starts -->
    <div class="sticky-header header-section ">
        <div class="header-left">
            <!--toggle button start-->
            <button id="showLeftPush"><i class="fa fa-bars"></i></button>
            <!--toggle button end-->
            <!--logo -->
            <div class="logo">
                <a href="index.html">
                    <h1>S-B-T</h1>
                    <span>后台管理</span>
                </a>
            </div>
            <!--//logo-->
            <!--search-box-->
            <div class="search-box">
                <form class="input">
                    <input class="sb-search-input input__field--madoka" placeholder="搜索..." type="search" id="input-31" />
                    <label class="input__label" for="input-31">
                        <svg class="graphic" width="100%" height="100%" viewBox="0 0 404 77" preserveAspectRatio="none">
                            <path d="m0,0l404,0l0,77l-404,0l0,-77z"/>
                        </svg>
                    </label>
                </form>
            </div><!--//end-search-box-->
            <div class="clearfix"> </div>
        </div>
        <div class="header-right">
            <div class="profile_details_left"><!--notifications of menu start -->
                <ul class="nofitications-dropdown">
                    <li class="dropdown head-dpdn">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-envelope"></i><span class="badge">3</span></a>
                        <ul class="dropdown-menu">
                            <li>
                                <div class="notification_header">
                                    <h3>你收到了三条信息</h3>
                                </div>
                            </li>
                            <li><a href="#">
                                <div class="user_img"><img src="${pageContext.request.contextPath}/assets/images/1.png" alt=""></div>
                                <div class="notification_desc">
                                    <p>Lorem ipsum dolor amet</p>
                                    <p><span>1 hour ago</span></p>
                                </div>
                                <div class="clearfix"></div>
                            </a></li>
                            <li class="odd"><a href="#">
                                <div class="user_img"><img src="${pageContext.request.contextPath}/assets/images/2.png" alt=""></div>
                                <div class="notification_desc">
                                    <p>Lorem ipsum dolor amet </p>
                                    <p><span>1 hour ago</span></p>
                                </div>
                                <div class="clearfix"></div>
                            </a></li>
                            <li><a href="#">
                                <div class="user_img"><img src="${pageContext.request.contextPath}/assets/images/3.png" alt=""></div>
                                <div class="notification_desc">
                                    <p>Lorem ipsum dolor amet </p>
                                    <p><span>1 hour ago</span></p>
                                </div>
                                <div class="clearfix"></div>
                            </a></li>
                            <li>
                                <div class="notification_bottom">
                                    <a href="#">查看所有信息</a>
                                </div>
                            </li>
                        </ul>
                    </li>
                    <li class="dropdown head-dpdn">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-bell"></i><span class="badge blue">3</span></a>
                        <ul class="dropdown-menu">
                            <li>
                                <div class="notification_header">
                                    <h3>你收到了三条通知</h3>
                                </div>
                            </li>
                            <li><a href="#">
                                <div class="user_img"><img src="${pageContext.request.contextPath}/assets/images/2.png" alt=""></div>
                                <div class="notification_desc">
                                    <p>Lorem ipsum dolor amet</p>
                                    <p><span>1 hour ago</span></p>
                                </div>
                                <div class="clearfix"></div>
                            </a></li>
                            <li class="odd"><a href="#">
                                <div class="user_img"><img src="${pageContext.request.contextPath}/assets/images/1.png" alt=""></div>
                                <div class="notification_desc">
                                    <p>Lorem ipsum dolor amet </p>
                                    <p><span>1 hour ago</span></p>
                                </div>
                                <div class="clearfix"></div>
                            </a></li>
                            <li><a href="#">
                                <div class="user_img"><img src="${pageContext.request.contextPath}/assets/images/3.png" alt=""></div>
                                <div class="notification_desc">
                                    <p>Lorem ipsum dolor amet </p>
                                    <p><span>1 hour ago</span></p>
                                </div>
                                <div class="clearfix"></div>
                            </a></li>
                            <li>
                                <div class="notification_bottom">
                                    <a href="#">查看所有通知</a>
                                </div>
                            </li>
                        </ul>
                    </li>
                    <li class="dropdown head-dpdn">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-tasks"></i><span class="badge blue1">15</span></a>
                        <ul class="dropdown-menu">
                            <li>
                                <div class="notification_header">
                                    <h3>你有八个挂起的任务</h3>
                                </div>
                            </li>
                            <li><a href="#">
                                <div class="task-info">
                                    <span class="task-desc">Database update</span><span class="percentage">40%</span>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="progress progress-striped active">
                                    <div class="bar yellow" style="width:40%;"></div>
                                </div>
                            </a></li>
                            <li><a href="#">
                                <div class="task-info">
                                    <span class="task-desc">Dashboard done</span><span class="percentage">90%</span>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="progress progress-striped active">
                                    <div class="bar green" style="width:90%;"></div>
                                </div>
                            </a></li>
                            <li><a href="#">
                                <div class="task-info">
                                    <span class="task-desc">Mobile App</span><span class="percentage">33%</span>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="progress progress-striped active">
                                    <div class="bar red" style="width: 33%;"></div>
                                </div>
                            </a></li>
                            <li><a href="#">
                                <div class="task-info">
                                    <span class="task-desc">Issues fixed</span><span class="percentage">80%</span>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="progress progress-striped active">
                                    <div class="bar  blue" style="width: 80%;"></div>
                                </div>
                            </a></li>
                            <li>
                                <div class="notification_bottom">
                                    <a href="#">查看所有任务</a>
                                </div>
                            </li>
                        </ul>
                    </li>
                </ul>
                <div class="clearfix"> </div>
            </div>
            <!--notification menu end -->
            <div class="profile_details">
                <ul>
                    <li class="dropdown profile_details_drop">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                            <div class="profile_img">
                                <span class="prfil-img"><img src="${pageContext.request.contextPath}/assets/images/a.png" alt=""> </span>
                                <div class="user-name">
                                    <p>周大帅</p>
                                    <span>Administrator</span>
                                </div>
                                <i class="fa fa-angle-down lnr"></i>
                                <i class="fa fa-angle-up lnr"></i>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                        <ul class="dropdown-menu drp-mnu">
                            <li> <a href="#"><i class="fa fa-cog"></i> Settings</a> </li>
                            <li> <a href="#"><i class="fa fa-user"></i> Profile</a> </li>
                            <li> <a href="#"><i class="fa fa-sign-out"></i> Logout</a> </li>
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="clearfix"> </div>
        </div>
        <div class="clearfix"> </div>
    </div>
    <!-- //header-ends -->
    <!-- main content start-->
    <div id="page-wrapper">
        <div class="main-page">
            <div class="row-one">
                <div class="col-md-4 widget">
                    <div class="stats-left ">
                        <h5>今日</h5>
                        <h4>销售量</h4>
                    </div>
                    <div class="stats-right">
                        <label> ${sales}</label>
                    </div>
                    <div class="clearfix"> </div>
                </div>
                <div class="col-md-4 widget states-mdl">
                    <div class="stats-left">
                        <h5>
                            今日
                        </h5>
                        <h4>浏览量</h4>
                    </div>
                    <div class="stats-right">
                        <label> ${browNum}</label>
                    </div>
                    <div class="clearfix"> </div>
                </div>
                <div class="col-md-4 widget states-last">
                    <div class="stats-left">
                        <h5>今日</h5>
                        <h4>订单量</h4>
                    </div>
                    <div class="stats-right">
                        <label>${orders}</label>
                    </div>
                    <div class="clearfix"> </div>
                </div>
                <div class="clearfix"> </div>
            </div>
            <div class="charts">
                <div class="col-md-4 charts-grids widget">
                    <h4 class="title">柱状图分析</h4>
                    <canvas id="bar" height="300" width="400"> </canvas>
                </div>
                <div class="col-md-4 charts-grids widget states-mdl">
                    <h4 class="title">线状图分析</h4>
                    <canvas id="line" height="300" width="400"> </canvas>
                </div>
                <div class="col-md-4 charts-grids widget">
                    <h4 class="title">饼状图分析</h4>
                    <canvas id="pie" height="300" width="400"> </canvas>
                </div>
                <div class="clearfix"> </div>
                <script>
                    var barChartData = {
                        labels : ["一月","二月","三月","四月","五月","六月","七月"],
                        datasets : [
                            {
                                fillColor : "rgba(233, 78, 2, 0.9)",
                                strokeColor : "rgba(233, 78, 2, 0.9)",
                                highlightFill: "#e94e02",
                                highlightStroke: "#e94e02",
                                data : [65,59,90,81,56,55,40]
                            },
                            {
                                fillColor : "rgba(79, 82, 186, 0.9)",
                                strokeColor : "rgba(79, 82, 186, 0.9)",
                                highlightFill: "#4F52BA",
                                highlightStroke: "#4F52BA",
                                data : [40,70,55,20,45,70,60]
                            }
                        ]

                    };
                    var lineChartData = {
                        labels : ["一月","二月","三月","四月","五月","六月","七月"],
                        datasets : [
                            {
                                fillColor : "rgba(242, 179, 63, 1)",
                                strokeColor : "#F2B33F",
                                pointColor : "rgba(242, 179, 63, 1)",
                                pointStrokeColor : "#fff",
                                data : [70,60,72,61,75,59,80]

                            },
                            {
                                fillColor : "rgba(97, 100, 193, 1)",
                                strokeColor : "#6164C1",
                                pointColor : "rgba(97, 100, 193,1)",
                                pointStrokeColor : "#9358ac",
                                data : [50,65,51,67,52,64,50]

                            }
                        ]

                    };
                    var pieData = [
                        {
                            value: 90,
                            color:"rgba(233, 78, 2, 1)",
                            label: "第一季度"
                        },
                        {
                            value : 50,
                            color : "rgba(242, 179, 63, 1)",
                            label: "第二季度"
                        },
                        {
                            value : 60,
                            color : "rgba(88, 88, 88,1)",
                            label: "第三季度"
                        },
                        {
                            value : 40,
                            color : "rgba(79, 82, 186, 1)",
                            label: "第四季度"
                        }

                    ];

                    new Chart(document.getElementById("line").getContext("2d")).Line(lineChartData);
                    new Chart(document.getElementById("bar").getContext("2d")).Bar(barChartData);
                    new Chart(document.getElementById("pie").getContext("2d")).Pie(pieData);

                </script>

            </div>
            <div class="row">
                <div class="col-md-4 stats-info widget">
                    <div class="stats-title">
                        <h4 class="title">销售占比</h4>
                    </div>
                    <div class="stats-body">
                        <ul class="list-unstyled">
                            <c:if test="${sp1!=null}">
                                <li>${sp1.categoryName} <span class="pull-right">${sp1.percent}</span>
                                    <div class="progress progress-striped active progress-right">
                                        <div class="bar green" style="width:${sp1.percent};"></div>
                                    </div>
                                </li>
                            </c:if>

                            <c:if test="${sp2!=null}">
                                <li>${sp2.categoryName} <span class="pull-right">${sp2.percent}</span>
                                    <div class="progress progress-striped active progress-right">
                                        <div class="bar yellow" style="width:${sp2.percent};"></div>
                                    </div>
                                </li>
                            </c:if>

                            <c:if test="${sp3!=null}">
                                <li>${sp3.categoryName}<span class="pull-right">${sp3.percent}</span>
                                    <div class="progress progress-striped active progress-right">
                                        <div class="bar red" style="width:${sp3.percent};"></div>
                                    </div>
                                </li>
                            </c:if>

                            <c:if test="${sp4!=null}">
                                <li>${sp4.categoryName} <span class="pull-right">${sp4.percent}</span>
                                    <div class="progress progress-striped active progress-right">
                                        <div class="bar blue" style="width:${sp4.percent};"></div>
                                    </div>
                                </li>
                            </c:if>

                            <c:if test="${sp5!=null}">
                                <li>${sp5.categoryName}<span class="pull-right">${sp5.percent}</span>
                                    <div class="progress progress-striped active progress-right">
                                        <div class="bar light-blue" style="width:${sp5.percent};"></div>
                                    </div>
                                </li>
                            </c:if>

                            <c:if test="${other!=null}">
                                <li class="last">其他 <span class="pull-right">${other}</span>
                                    <div class="progress progress-striped active progress-right">
                                        <div class="bar orange" style="width:${other};"></div>
                                    </div>
                                </li>
                            </c:if>
                        </ul>
                    </div>
                </div>
                <div class="col-md-8 stats-info stats-last widget-shadow">
                    <table class="table stats-table ">
                        <thead>
                        <tr>
                            <th>销售排行</th>
                            <th>产品名</th>
                            <th>销售状态</th>
                            <th>销售进步</th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:if test="${sp1!=null}">
                            <tr>
                                <th scope="row">1</th>
                                <td>${sp1.categoryName}</td>
                                <td><span class="label label-success">热销</span></td>
                                <td><h5>85% <i class="fa fa-level-up"></i></h5></td>
                            </tr>
                        </c:if>

                        <c:if test="${sp2!=null}">
                            <tr>
                                <th scope="row">2</th>
                                <td>${sp2.categoryName}</td>
                                <td><span class="label label-warning">新产品</span></td>
                                <td><h5>35% <i class="fa fa-level-up"></i></h5></td>
                            </tr>
                        </c:if>

                        <c:if test="${sp3!=null}">
                            <tr>
                                <th scope="row">3</th>
                                <td>${sp3.categoryName}</td>
                                <td><span class="label label-danger">亏损</span></td>
                                <td><h5  class="down">40% <i class="fa fa-level-down"></i></h5></td>
                            </tr>
                        </c:if>

                        <c:if test="${sp4!=null}">
                            <tr>
                                <th scope="row">4</th>
                                <td>${sp4.categoryName}</td>
                                <td><span class="label label-info">热销</span></td>
                                <td><h5>100% <i class="fa fa-level-up"></i></h5></td>
                            </tr>
                        </c:if>

                        <c:if test="${sp5!=null}">
                            <tr>
                                <th scope="row">5</th>
                                <td>${sp5.categoryName}</td>
                                <td><span class="label label-success">亏损</span></td>
                                <td><h5 class="down">10% <i class="fa fa-level-down"></i></h5></td>
                            </tr>
                        </c:if>
                        </tbody>
                    </table>
                </div>
                <div class="clearfix"> </div>
            </div>

            <div class="clearfix"> </div>
        </div>
    </div>
    <!--footer-->
    <div class="footer">
        <p>Copyright &copy; 2016.Company name All rights reserved.<a target="_blank" href="http://www.17sucai.com/">Designed By SBT</a></p>
    </div>
    <!--//footer-->
</div>
<!-- Classie -->
<script src="${pageContext.request.contextPath}/assets/js/classie.js"></script>
<script>
    var menuLeft = document.getElementById( 'cbp-spmenu-s1' ),
        showLeftPush = document.getElementById( 'showLeftPush' ),
        body = document.body;

    showLeftPush.onclick = function() {
        classie.toggle( this, 'active' );
        classie.toggle( body, 'cbp-spmenu-push-toright' );
        classie.toggle( menuLeft, 'cbp-spmenu-open' );
        disableOther( 'showLeftPush' );
    };


    function disableOther( button ) {
        if( button !== 'showLeftPush' ) {
            classie.toggle( showLeftPush, 'disabled' );
        }
    }
</script>
<!--scrolling js-->
<script src="${pageContext.request.contextPath}/assets/js/jquery.nicescroll.js"></script>
<script src="${pageContext.request.contextPath}/assets/js/scripts.js"></script>
<!--//scrolling js-->
<!-- Bootstrap Core JavaScript -->
<script src="${pageContext.request.contextPath}/assets/js/bootstrap.js"> </script>
</body>
</html>