<%@ page import="pri.pojo.User" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <title>订单管理</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
    <!-- Bootstrap Core CSS -->
    <link href="${pageContext.request.contextPath}/assets/css/bootstrap.css" rel='stylesheet' type='text/css' />
    <!-- Custom CSS -->
    <link href="${pageContext.request.contextPath}/assets/css/style.css" rel='stylesheet' type='text/css' />
    <!-- font CSS -->
    <!-- font-awesome icons -->
    <link href="${pageContext.request.contextPath}/assets/css/font-awesome.css" rel="stylesheet">
    <!-- //font-awesome icons -->
    <!-- js-->
    <script src="${pageContext.request.contextPath}/assets/js/jquery-1.11.1.min.js"></script>
    <script src="${pageContext.request.contextPath}/assets/js/modernizr.custom.js"></script>
    <!--webfonts-->
    <link href='http://fonts.useso.com/css?family=Roboto+Condensed:400,300,300italic,400italic,700,700italic' rel='stylesheet' type='text/css'>
    <!--//webfonts-->
    <!--animate-->
    <link href="${pageContext.request.contextPath}/assets/css/animate.css" rel="stylesheet" type="text/css" media="all">
    <script src="${pageContext.request.contextPath}/assets/js/wow.min.js"></script>
    <script>
        new WOW().init();
    </script>
    <!--//end-animate-->
    <!-- chart -->
    <script src="${pageContext.request.contextPath}/assets/js/Chart.js"></script>
    <!-- //chart -->
    <!--Calender-->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/clndr.css" type="text/css" />
    <script src="${pageContext.request.contextPath}/assets/js/underscore-min.js" type="text/javascript"></script>
    <script src= "${pageContext.request.contextPath}/assets/js/moment-2.2.1.js" type="text/javascript"></script>
    <script src="${pageContext.request.contextPath}/assets/js/clndr.js" type="text/javascript"></script>
    <script src="${pageContext.request.contextPath}/assets/js/site.js" type="text/javascript"></script>
    <!--End Calender-->
    <!-- Metis Menu -->
    <script src="${pageContext.request.contextPath}/assets/js/metisMenu.min.js"></script>
    <script src="${pageContext.request.contextPath}/assets/js/custom.js"></script>
    <link href="${pageContext.request.contextPath}/assets/css/custom.css" rel="stylesheet">
    <!--//Metis Menu -->
    <!--layui-->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/layui/css/layui.css">
    <script src="${pageContext.request.contextPath}/assets/layui/layui.js"></script>
    <style type="text/css">
        .layui-table-cell {
            height: auto;
            padding: 0;
            margin: 0;
        }
        .layui-table tbody tr:hover, .layui-table thead tr, .layui-table-click, .layui-table-header, .layui-table-hover, .layui-table-mend, .layui-table-patch, .layui-table-tool, .layui-table[lay-even] tr:nth-child(even) {
            background-color: transparent;
        }
        .layui-table{
            height: auto;
        }


        .innertable{
            width: 100%;
        }
        .innertable tr td{
            border: none;
            height: auto;
            word-break: break-all;
            word-wrap:break-word;
            white-space:normal;
            text-align: center;
        }

        .layui-form-item{
            margin: 50px;
        }

    </style>
</head>
<body class="cbp-spmenu-push">
<%@ include file="../head.jsp"%>








<div id="page-wrapper">

    <div id="container" style="margin-left: 40px" >
        <div class="layui-tab layui-tab-brief">
            <ul class="layui-tab-title">
                <li class="layui-this">近三个月订单</li>
                <li>等待买家付款</li>
                <li>等待发货</li>
                <li>已发货</li>
                <li>需要评价</li>
                <li>成功的订单</li>
                <li>三个月以前的订单</li>
            </ul>
            <div class="layui-tab-content">
                <div class="layui-tab-item layui-show"><table id="recent-order" lay-filter="test"></table></div>
                <div class="layui-tab-item"><table id="waitpay-order" lay-filter="test"></table></div>
                <div class="layui-tab-item"><table id="waitsend-order" lay-filter="test"></table></div>
                <div class="layui-tab-item"><table id="waitconfirm-order" lay-filter="test"></table></div>
                <div class="layui-tab-item"><table id="waitcomment-order" lay-filter="test"></table></div>
                <div class="layui-tab-item"><table id="success-order" lay-filter="test"></table></div>
                <div class="layui-tab-item"><table id="history-order" lay-filter="test"></table></div>
            </div>
        </div>
    </div>


</div>
<div class="modal fade" tabindex="-1" role="dialog" id="myModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">发货</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label>收货地址</label>
                    <div id="address"></div>
                </div>
                <label style="margin-top: 20px">选择物流</label>
                <select class="form-control" id="com">
                    <option value="1">顺丰</option>
                    <option value="2">圆通</option>
                    <option value="3">韵达</option>
                    <option value="4">中通</option>
                    <option value="5">申通</option>
                </select>
                <div class="form-group" style="margin-top: 30px">
                    <label for="expressNum">物流号</label>
                    <input type="number" class="form-control" id="expressNum" placeholder="请输入物流号">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                <input type="hidden" value="" id="myoid">
                <button type="button" class="btn btn-primary" onclick="confirm()">确认</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->












<%@ include file="../foot.jsp"%>
<!--//footer--><!-- Classie -->
<script src="${pageContext.request.contextPath}/assets/js/classie.js"></script>
<script>
    var menuLeft = document.getElementById( 'cbp-spmenu-s1' ),
        showLeftPush = document.getElementById( 'showLeftPush' ),
        body = document.body;

    showLeftPush.onclick = function() {
        classie.toggle( this, 'active' );
        classie.toggle( body, 'cbp-spmenu-push-toright' );
        classie.toggle( menuLeft, 'cbp-spmenu-open' );
        disableOther( 'showLeftPush' );
    };


    function disableOther( button ) {
        if( button !== 'showLeftPush' ) {
            classie.toggle( showLeftPush, 'disabled' );
        }
    }
</script>
<!--scrolling js-->
<script src="${pageContext.request.contextPath}/assets/js/jquery.nicescroll.js"></script>
<script src="${pageContext.request.contextPath}/assets/js/scripts.js"></script>
<!--//scrolling js-->
<!-- Bootstrap Core JavaScript -->
<script src="${pageContext.request.contextPath}/assets/js/bootstrap.js"> </script>
<script>
    //注意：选项卡 依赖 element 模块，否则无法进行功能性操作
    layui.use('element', function(){
        var element = layui.element;
    });
</script>
<script>
    layui.use('table', function(){
        var table = layui.table;
        //第一个实例
        table.render({
            elem: '#recent-order'
            ,height: 700
            ,url: '${pageContext.request.contextPath}/order/all?op=recent' //数据接口
            ,page: true //开启分页
            ,cols: [[ //表头
                {field: 'oid', title: '订单号',width:120,align:'center'}
                ,{field: 'time', title: '状态时间',width:170,align:'center'}
                ,{field: 'product', title: '商品信息',templet: '#product_info',align:'center'}
                ,{field: 'customer', title: '买家', width: 177,align:'center'}
                ,{field: 'status', title: '交易状态', width: 80, sort: true,align:'center'}
                ,{field: 'real_price', title: '实收款', width: 80, sort: true,align:'center'}
                ,{fixed: 'right', width:150, align:'center', toolbar: '#only_order'}
            ]]
        });
    });


    layui.use('table', function(){
        var table = layui.table;
        //第一个实例
        table.render({
            elem: '#waitpay-order'
            ,height: 700
            ,url: '${pageContext.request.contextPath}/order/all?op=waitpay' //数据接口
            ,page: true //开启分页
            ,cols: [[ //表头
                {field: 'oid', title: '订单号',width:120,align:'center'}
                ,{field: 'time', title: '状态时间',width:170,align:'center'}
                ,{field: 'product', title: '商品信息',templet: '#product_info',align:'center'}
                ,{field: 'customer', title: '买家', width: 177,align:'center'}
                ,{field: 'status', title: '交易状态', width: 80, sort: true,align:'center'}
                ,{field: 'real_price', title: '实收款', width: 80, sort: true,align:'center'}
                ,{fixed: 'right', width:150, align:'center', toolbar: '#normal_order'}
            ]]
        });
    });

    var tableIns;
    layui.use('table', function(){
        var table = layui.table;
        //第一个实例
        tableIns = table.render({
            elem: '#waitsend-order'
            ,height: 700
            ,url: '${pageContext.request.contextPath}/order/all?op=waitsend' //数据接口
            ,page: true //开启分页
            ,cols: [[ //表头
                {field: 'oid', title: '订单号',width:120,align:'center'}
                ,{field: 'time', title: '状态时间',width:170,align:'center'}
                ,{field: 'product', title: '商品信息',templet: '#product_info',align:'center'}
                ,{field: 'customer', title: '买家', width: 177,align:'center'}
                ,{field: 'status', title: '交易状态', width: 80, sort: true,align:'center'}
                ,{field: 'real_price', title: '实收款', width: 80, sort: true,align:'center'}
                ,{fixed: 'right', width:150, align:'center', toolbar: '#send_order'}
            ]]
        });
    });

    layui.use('table', function(){
        var table = layui.table;
        //第一个实例
        table.render({
            elem: '#waitconfirm-order'
            ,height: 700
            ,url: '${pageContext.request.contextPath}/order/all?op=waitconfirm' //数据接口
            ,page: true //开启分页
            ,cols: [[ //表头
                {field: 'oid', title: '订单号',width:120,align:'center'}
                ,{field: 'time', title: '状态时间',width:170,align:'center'}
                ,{field: 'product', title: '商品信息',templet: '#product_info',align:'center'}
                ,{field: 'customer', title: '买家', width: 177,align:'center'}
                ,{field: 'status', title: '交易状态', width: 80, sort: true,align:'center'}
                ,{field: 'real_price', title: '实收款', width: 80, sort: true,align:'center'}
                ,{fixed: 'right', width:150, align:'center', toolbar: '#only_order'}
            ]]
        });
    });

    layui.use('table', function(){
        var table = layui.table;
        //第一个实例
        table.render({
            elem: '#waitcomment-order'
            ,height: 700
            ,url: '${pageContext.request.contextPath}/order/all?op=waitcomment' //数据接口
            ,page: true //开启分页
            ,cols: [[ //表头
                {field: 'oid', title: '订单号',width:120,align:'center'}
                ,{field: 'time', title: '状态时间',width:170,align:'center'}
                ,{field: 'product', title: '商品信息',templet: '#product_info',align:'center'}
                ,{field: 'customer', title: '买家', width: 177,align:'center'}
                ,{field: 'status', title: '交易状态', width: 80, sort: true,align:'center'}
                ,{field: 'real_price', title: '实收款', width: 80, sort: true,align:'center'}
                ,{fixed: 'right', width:150, align:'center', toolbar: '#only_order'}
            ]]
        });
    });

    layui.use('table', function(){
        var table = layui.table;
        //第一个实例
        table.render({
            elem: '#success-order'
            ,height: 700
            ,url: '${pageContext.request.contextPath}/order/all?op=success' //数据接口
            ,page: true //开启分页
            ,cols: [[ //表头
                {field: 'oid', title: '订单号',width:120,align:'center'}
                ,{field: 'time', title: '状态时间',width:170,align:'center'}
                ,{field: 'product', title: '商品信息',templet: '#product_info',align:'center'}
                ,{field: 'customer', title: '买家', width: 177,align:'center'}
                ,{field: 'status', title: '交易状态', width: 80, sort: true,align:'center'}
                ,{field: 'real_price', title: '实收款', width: 80, sort: true,align:'center'}
                ,{fixed: 'right', width:150, align:'center', toolbar: '#only_order'}
            ]]
        });
    });

    layui.use('table', function(){
        var table = layui.table;
        //第一个实例
        table.render({
            elem: '#history-order'
            ,height: 700
            ,url: '${pageContext.request.contextPath}/order/all?op=history' //数据接口
            ,page: true //开启分页
            ,cols: [[ //表头
                {field: 'oid', title: '订单号',width:120,align:'center'}
                ,{field: 'time', title: '状态时间',width:170,align:'center'}
                ,{field: 'product', title: '商品信息',templet: '#product_info',align:'center'}
                ,{field: 'customer', title: '买家', width: 177,align:'center'}
                ,{field: 'status', title: '交易状态', width: 80, sort: true,align:'center'}
                ,{field: 'real_price', title: '实收款', width: 80, sort: true,align:'center'}
                ,{fixed: 'right', width:150, align:'center', toolbar: '#only_order'}
            ]]
        });
    });

</script>
<script type="text/html" id="product_info">
    <table class="innertable">
        {{#  var info = eval(d.product)}}

        {{# layui.each(info,function(index,item){ }}
        <tr style="padding: 50px">
            <td>{{ item.product_name }}</td>
            <td style="width: 100px">{{ item.product_price }}</td>
            <td style="width: 30px">x {{ item.product_count }}</td>
        </tr>
        <tr ><td></td><td></td><td></td></tr>
        {{# }); }}
    </table>
</script>

<script type="text/html" id="only_order">
    <a class="layui-btn layui-btn" lay-event="detail">查看</a>
</script>

<script type="text/html" id="normal_order">
    <a class="layui-btn layui-btn" lay-event="detail">查看</a>
    <a class="layui-btn layui-btn-danger layui-btn" lay-event="del">删除</a>
</script>

<script type="text/html" id="send_order">
    <a class="layui-btn layui-btn" lay-event="detail">查看</a>
    <a class="layui-btn layui-btn layui-btn-warm" lay-event="detail" href="javascript:void(0)" onclick="send('{{d.oid}}')">发货</a>
</script>
<script>
    layui.use('layer', function(){
        var layer = layui.layer;
    });

    function send(oid){
        $.ajax({
                url: "${pageContext.request.contextPath}/order/getrec",
                type: "POST",
                async: true,
                data: {oid:oid},
                dataType: "json",
                success:function (data) {
                    $("#myoid").val(oid);
                    $("#address").html(data.country+","+data.province+","+data.city+","+data.street+","+data.detail+","+data.recName+","+data.recPhone);
                    $("#myModal").modal("show");  //手动开启
                },
                error:function () {
                    alert("网络异常");
                }
            }
        )

    }

    function confirm() {
        var oid = $("#myoid").val();
        var com = $("#com").val();
        var express = $("#expressNum").val();
        $.ajax({
                url: "${pageContext.request.contextPath}/order/send",
                type: "POST",
                async: true,
                data: {oid:oid,com_id:com,express_num:express},
                dataType: "json",
                success:function (data) {
                    if (data.code == "0"){
                        alert("发货成功");
                        $("#myModal").modal("hide");  //手动关闭
                        tableIns.reload({page: {curr: 1 }});
                    }else {
                        alert("发货失败");
                    }
                },
                error:function () {
                    alert("网络异常");
                }
            }
        )

    }


</script>
<script>
    layui.use('layer', function(){
        var layer = layui.layer;
    });

    function send(oid){
        $.ajax({
                url: "${pageContext.request.contextPath}/order/getrec",
                type: "POST",
                async: true,
                data: {oid:oid},
                dataType: "json",
                success:function (data) {
                    $("#myoid").val(oid);
                    $("#address").html(data.country+","+data.province+","+data.city+","+data.street+","+data.detail+","+data.recName+","+data.recPhone);
                    $("#myModal").modal("show");  //手动开启
                },
                error:function () {
                    alert("网络异常");
                }
            }
        )

    }

    function confirm() {
        var oid = $("#myoid").val();
        var com = $("#com").val();
        var express = $("#expressNum").val();
        $.ajax({
                url: "${pageContext.request.contextPath}/order/send",
                type: "POST",
                async: true,
                data: {oid:oid,com_id:com,express_num:express},
                dataType: "json",
                success:function (data) {
                    if (data.code == "0"){
                        alert("发货成功");
                        $("#myModal").modal("hide");  //手动关闭
                        tableIns.reload({page: {curr: 1 }});
                    }else {
                        alert("发货失败");
                    }
                },
                error:function () {
                    alert("网络异常");
                }
            }
        )

    }


</script>

</body>
</html>
