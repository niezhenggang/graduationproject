# 数码垂直电商系统（后台）

#### 介绍
四川轻化工大学16级软件工程系*帅不谈*小组基于SSM的课程设计 数码垂直电商系统 功能已经比较完善 只有一些小bug 大家可以用来二次开发 也欢迎提出意见 本仓库放的是后系统 前台系统地址：[https://gitee.com/sclcsm/projectsign](https://gitee.com/sclcsm/projectdesign/)

#### 软件架构
本次使用技术栈
Spring + SpringMVC + Mybatis + Mysql5.6

#### 已实现功能
1. 商品销量统计
2. 商品上下架
3. 订单发货
4. 商品发布

#### 已知BUG
1. 登陆模块不严谨
2. session判断登陆没有做
3. 因为时间匆忙 很多操作没有实现鉴权 大家可以自己加上

#### 安装教程

1. 克隆到本地
2. 使用IDEA或ECLIPSE直接打开
3. 把sql文件导入数据库（sql文件在前台的仓库里 因为前后台用的一个数据库 就不放两个文件了）
4. 配置好数据库池（applicationContext.xml）
5. 配置好Tomcat 
6. 直接运行

#### 使用说明

1. 因为时间匆忙 会有一些bug出现 欢迎大家修改 指出
2. 默认账号（后台）admin admin123

#### 参与贡献

1. Change
2. Professor_Chen
3. nie正刚
4. 白马神
5. 廖浩辉
